F-IMM-08 
FORMULAIRE 
DECLARATION DE RENONCIATION 
AU CONTRÔLE TECHNIQUE 
Version 
2.2 
03.12.08 
 
SERVICE 
IMMATRICULATION
 
Vérification: Validation: Page  1 / 2 
 
 
 
En vertu des dispositions afférentes du paragraphe 1
er
 de l’article 4 bis de la loi modifiée du 14 février 
1955 concernant la réglementation de la circulation sur toutes les voies publiques, le soussigné 
 
Nom et Prénom 
…………………………………………..……………………………… 
 
Date de naissance ……………     ………..     ……….        ………………………… 
 
    (année)               (mois)            (jour)                   (numéro-matricule) 
 
ayant introduit une demande pour l’immatriculation en son nom du véhicule: 
 
Numéro d’immatriculation 
…………………….  
Numéro de châssis 
…………………………………………………………… 
 
déclare renoncer de son propre gré au contrôle technique de ce véhicule en relation avec la 
transcription de celui-ci à son nom. 
 
 
Par ailleurs le déclarant soussigné confirme que le véhicule pré-désigné n’a pas fait l’objet, depuis 
son dernier passage au contrôle technique, d’une modification de nature à modifier une des 
caractéristiques techniques figurant soit sur son procès verbal de réception, soit sur son\ certificat de 
conformité, soit sur son certificat d’immatriculation. 
 
 
En outre, le déclarant s’engage à assurer que les plaques d’immatriculation apposées sur le 
véhicule sont conformes, en ce qui concerne leur 
état et leur fixation, à toutes les exigences 
applicables du règlement grand-ducal modifié du 17 juin 2003 relatif à l’identification des véhicules 
routiers, à leurs plaques d’immatriculation et aux modalités d’attribution de leurs numéros 
d’immatriculation et que ces plaques affichent le 
numéro d’immatriculation correct. 
 
 
à compléter lorsque le véhicule est immatriculé au nom d’une personne morale 
 
 
Le déclarant confirme être valablement mandaté pour agir et signer au nom: 
 □ de la société 
 □ de l’association 
 □ du club ………………………………………………………………………………………………………. 
 
 désignation de la société, de l’association, du club 
 
 
 
 
 
 
…………………………………………, le  ………………….                 ……………..………………………………….. 
 
 localité date signature du nouveau propriétaire 
 
 
Prière de joindre une copie d'un document d’identité du déclarant 
 
 
---------------------------------------------------- 
Deutsche Fassung: siehe umseitig 
---------------------------------------------------- 
F-IMM-08 
FORMULAR 
VERZICHTSERKLÄRUNG BETR. DIE TECHNISCHE KONTROLLE 
Version 
2.2 
03.12.08 
 
SERVICE 
IMMATRICULATION
 
Vérification: Validation: Page  2 / 2 
 
 
Kraft der diesbezüglichen Bestimmungen von Paragraph 1 von Artikel 4 bis des abgeänderten 
Gesetzes vom 14 Februar 1955 betreffend die Regelung des Verkehrs auf allen /öffentlichen 
Strassen, erklärt der Unterzeichnete 
 
Name und Vorname 
……………………………………………….......………...... 
 
Geburtsdatum ……………     ………..     …..….        ………\…............ 
 
    (Jahr)                (Monat)          (Tag)                    (Matrikel) 
 
im Zusammenhang mit dem Antrag zur Anmeldung auf seinen Namen des Fahrzeugs: 
 
Zulassungsnummer 
……………………  
Fahrgestellnummer 
………………………………………………………… 
 
im Rahmen der Umschreibung des vorbeschriebenen Fahrzeugs auf seinen Nam\en aus 
freiem Willen auf die diesbezügliche technische Untersuchung des Fahrzeugs zu verzichten. 
 
 
Zudem bestätigt der Unterzeichnete, dass an dem oben bezeichneten Fahrzeug seit dessen letzter 
Vorführung zur technischen Untersuchung keine Änderungen vorgenommen worden sind, die 
dessen technischen Merkmalen, die auf dem Abnahmeprotokoll, der Konformitätsbescheinigung 
oder dem Zulassungsdokument eingetragen sind, nicht entsprechen würden. 
 
 
Ausserdem, bestätigt der Unterzeichnete dass er die volle Verantwortung dafür übernimmt, dass die 
auf dem Fahrzeug angebrachten 
Kennzeichentafeln ("Nummernschilder") in Beschaffenheit und 
Befestigung allen diesbezüglich geltenden Anforderungen des Grossherzoglichen Beschlusses vom 
17.
 Juni 2003 betreffend die Kennzeichnung der Strassen-Fahrzeuge, ihre Kennzeichentafeln und 
die Modalitäten zur Erteilung ihrer Zulassungsnummern entsprechen, und dass diese 
Kennzeichentafeln die 
korrekte Zulassungsnummer anzeigen. 
 
 
auszufüllen im Fall wo der Besitzer eine juristische Person ist 
 
 
Der Unterzeichnete erklärt, dass er berechtigt ist im Namen der nachfolgend bezeichneten juristischen Person zu handeln 
und zu unterzeichnen: 
 □ Firma 
 □ Vereinigung 
 □ Verein ………………………………………………………………………………………………………… 
 
 Bezeichnung der Firma, der Vereinigung, des Vereins 
 
 
 
 
 
…………………..…………………, den  ………………….                 ……………..………………………………….. 
 
 Ortschaft Datum Unterschrift des neuen Besitzers 
 
 
Bitte die Kopie eines Identitätsdokuments der unterzeichneten Person beifügen  
 
 
------------------------------------------------ 
Version française: voir au verso 
------------------------------------------------ 
 
