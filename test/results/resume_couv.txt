YOU
DRIVE
WE CARE 
youngacl@acl.lu
Veuillez noter que le présent texte n’est qu’un résumé de la couverture fournie aux termes de la 
police d’assurance. Votre club ou association dispose d’un exemplaire de la police d’assurance 
dans sa version intégrale, à laquelle il sera fait référence\ en cas de litige.
La présente police d’assurance prévoit l’indemnisation de tiers jusqu’à concurrence de 
CHF 2.500.000 pour des dommages corporels ou en cas de maladie et pour des dommages  
matériels consécutifs à un accident causé par l’assuré ou toute personne voyageant à bord du 
même véhicule que lui (onze passagers au maximum) et faisant du camping avec lui en dehors de 
son domicile habituel ou séjournant avec lui en location ou à l’hôtel. L’assurance est valable dans 
le monde entier, à l’exception des territoires soumis à la juridiction des Etats-Unis et du Canada.
Au cas où l’Assuré est contraint, pendant la durée de son séjour, de quitter temporairement 
les personnes qui l’accompagnent pour regagner son domicile, ces dernières continuent à être  
couvertes par l’assurance.
EXCLUSIONS: Sont exclus de la présente police d’assurance:
1. Les accidents causés par tout véhicule à propulsion mécanique, navires, bateaux (excepté les 
planches à voile et les bateaux sans moteur d’une longueur inférieure à cinq mètres) ou aéronefs, 
ou résultant d’une intoxication alimentaire, d’une pollution de l’air, de l’eau ou du sol, ou de 
l’organisation de toute manifestation de grande envergure et payante, ou occasionnés par des 
dégâts causés à la terre ou aux récoltes par le passage répété des campeurs ou l’installation de 
tentes, par l’abandon sur le terrain de détritus ou d’ordures ménagères, par des dégâts causés 
aux canalisations d’eau et de gaz ou à des câbles électriques souterrains et les dommages 
causés par tout acte de malveillance intentionnelle. 
2. Les accidents survenant à cause ou en conséquence des faits suivants, à savoir: guerre, 
invasion, acte d’ennemi étranger, hostilités (que la guerre ait été déclarée ou non), guerre  
civile, rébellion, révolution, insurrection, pouvoir militaire ou usurpation de pouvoir ou  
confiscation, nationalisation, réquisition, destruction ou dommages matériels par ou à l’ordre  
d’un Gouvernement, République ou Autorité locale.
 
3. Les dommages corporels, ou une maladie ou des dommages matériels subis, au cours de 
son activité professionnelle et résultant de celle-ci, par toute personne employée au service du  
titulaire de la Camping Card International en vertu d’un contrat de travail ou d’apprentissage 
conclu avec ce dernier. 
4. Les dommages matériels causés à des biens détenus ou occupés par l’Assuré, ou dont celui-ci 
assume la garde ou la surveillance.
Cette police est également sujette à une “Clause d’exclusion afférente à la contamination radioactive 
et aux dispositifs nucléaires explosibles” ainsi qu’à une “Clause d’exclusion afférente à l’amiante”.
Assurance responsabilité civile des campeurs  
de l’AIT & FIA (2006) Résumé de la couverture
YOU
DRIVE
WE CARE 
youngacl@acl.lu
DEMANDES D’INDEMNITES:
En cas de sinistre, l’assuré est tenu d’en informer immédiatement le club auquel il est affilié, par 
écrit et avec tous les renseignements utiles y afférents y compris toute information concernant 
d’autres éventuels contrats d’assurance de responsabilité civile. Aucune reconnaissance de  
responsabilité, ni aucune offre ou promesse de paiement ne peut être effectuée par l’assuré sans 
le consentement écrit de son club. Les Assureurs qui assurent ce dernier sont en droit d’assumer 
la défense ou d’intenter, au nom de l’assuré, tout recours contre un tiers, quel qu’il soit, et ont 
toute liberté en ce qui concerne la conduite des négociations ou poursuites en vue du règlement 
d’un sinistre quel qu’il soit. L’assuré est tenu de fournir aux Assureurs tous les renseignements 
et toute l’aide que ceux-ci pourraient raisonnablement lui demander. Les sinistres survenant  
pendant un séjour en location ou à l’hôtel sont assortis d’une franchise de CHF 100 ou de 
l’équivalent dans d’autres devises. 
VIOLATIONS DU CONTRAT ET CUMUL D’ASSURANCES:
Au cas où un membre d’une association assurée présente une demande d’indemnité sachant que 
celle-ci est fausse ou frauduleuse, soit quant au montant réclamé, soit d’une autre manière, les 
Assureurs sont libérés à l’égard de l’ayant droit de toute obligation contractuelle d’indemnisation. 
Au cas où les risques garantis par la présente police sont également couverts, en tout ou partie, 
par une autre assurance, la responsabilité des Assureurs sera limitée à la part de l’indemnisation 
qui n’est pas couverte par une autre assurance. 
