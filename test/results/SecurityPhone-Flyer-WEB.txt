Bascharage 
Cactus Bascharage
6, avenue de Luxembourg
Tél.: +352 27 888 360
Route d’Arlon
Tél.: +352 27 888 330
Bertrange
Siège VOXmobile
8, z.a.i. Bourmicht
Tél.: +352 27 888 300
Dudelange
67, av. Grande-Duchesse Charlotte
Tél.: +352 27 888 380
Esch/Alzette
39, rue de la Libération
Tél.: +352 27 888 320
Howald
Cactus Howald
3, rue des Scillas
Tél.: +352 27 888 340
Ingeldorf
Cactus Ingeldorf
4, rte d’Ettelbrück
Tél.: +352 27 888 350
Luxembourg-Ville
1-5, rue du Marché-aux-Herbes
Tél.: +352 27 888 310
Votre sécurité au bout des doigts
24h sur 24.
Secury Phone est la 1ère solution de 
géo-localisation au Luxembourg, réalisée 
grâce à l’étroite collaboration entre G4S, 
ACL et VOXmobile. Son call-center, géré 
par G4S, est opérationnel 24h sur 24, 365 
jours par an.  
HOME
1
1
2
Informations et vente
Secury Phone 
est vendu exclusivement avec un abonnement VOX.
La géolocalisation et les transferts d’appel vers le 112 sont 
opérés par G4S Group 4 Securicor.
Le prix de vente du Sécury Phone = 99€ avec l’option 10/24
219€ avec l’option 5/24
279€ avec l’option 5/12
339€ sans option
L’abonnement Sécury Phone est de 28€/mois et comprend 
un abonnement VOX All Inclusive 5 (5€/mois, incluant 50 mi-
nutes d’appel et SMS en national) ainsi que les prestations de 
G4S (23€/mois).
Secury Phone est en vente à :
ACL (Automobile Club du Luxembourg) 54, route de Longwy, 
Bertrange. Ouvert du lundi au vendredi de 8h30 à 18h.00. Info 
sur www.acl.lu ou +352 4500451.
ACL Diekirch. 20-22 rue de Stavelot. L-9280 Diekirch. Le bu-
reau de l‘ACL à Diekirch est ouvert du mardi au dimanche de 
10.00 à 18.00 heures
et aux VOX Shops suivants :
Où que vous vous trouviez au Lux-
embourg nous vous portons assis-
tance. Sur simple pression du bou-
ton d’urgence, le GPS  intégré du 
Secury Phone donne votre position 
exacte dans tout le pays. Grâce au 
téléphone intégré, notre opérateur 
prend contact avec vous afin de 
déterminer quelle est l’assistance 
dont vous avez besoin.
1
2
3
Appel GSM
2 numéros préprogrammés
Appel d’urgence
directement à la centrale G4S
Localisation GPS
géo-localisation
Secury Phone est 
un GSM, qui grâce 
à l’utilisation de 
la technique du 
GPS (Global Posi-
tioning Systems) 
permet à son por-
teur d’être locali-
sé à tout moment 
et ce à n‘importe 
quel endroit où 
il se trouve dans 
le pays . Secury 
Phone fonction-
ne comme une 
centrale d’alarme 
personnelle :
Il suffit d’appuyer 
sur un bouton 
pour parler avec 
une personne de 
confiance ou ap-
peler des secours.
•	aux enfants sur le chemin de l’école ou lors de leurs dé-
placements sportifs;
•	aux travailleurs isolés sur un chantier ou sur la route; 
•	aux personnes exerçant un métier dangereux;
•	aux personnes du 3ème âge qui sont seules et pour qui le 
GSM est trop compliqué;
•	aux personnes handicapées ou à motricité réduite.
Comment fonctionne Secury Phone ?
À quoi / à qui sert Secury Phone ?
Secury Phone  et d’une extrême simplicité d’utilisation. En cas 
de problème médical, d‘accident ou d‘incident quelconque,  
lorsque vous appuyez sur le bouton de secours, vous êtes 
immédiatement  en contact avec la centrale téléphonique de 
G4S qui se chargera , le cas échéant, de vous mettre en relati-
on avec les services de secours (112) tout en fournissant votre 
position exacte grâce au GPS intégré de votre Secury Phone.
Les deux autres boutons sont librement préprogrammables. 
Il suffira donc d’appuyer sur un des boutons d’appel pour 
être mis en communication avec la personne dont vous avez 
préprogrammé le numéro. Vous pouvez ensuite parler avec 
votre correspondant comme avec un GSM courant. Grâce à 
un accès sécurisé votre correspondant peut à tout moment 
connaître votre localisation.
1. Bouton SOS
2. Bouton 1 (programmable avec le no de votre choix)
3. Bouton 2 (programmable avec le n° de votre choix)
4. Bouton on/off
5. Antenne GPS
6. Haut parleur du GSM / sirène d’alarme
7. Boutons de contrôle du volume
8. Micro du GSM
1
2
3
4
5
6
7
8
Avec Secury Phone les parents peuvent à tout moment join-
dre leurs enfants grâce au GSM intégré mais également 
connaître leur position par le biais d‘un accès Internet sécu-
risé.
Les travailleurs isolés res-
tent en permanence en 
contact avec leur entre-
prise et peuvent à tout 
moment appeler du se-
cours ou demander du 
renfort.
Secury Phone est un sys-
tème d’alarme mobile 
idéal en cas de panne de voiture ou de cambriolage.
Les personnes ayant des risques de santé peuvent à tout mo-
ment recevoir de l’aide grâce à la localisation GPS ou entrer en 
contact avec un de leurs proches en appuyant sur le système 
d’appel GSM « one touch »
