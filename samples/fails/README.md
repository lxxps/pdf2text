# **Pdf2Text Fails**

This folder get all PDF files on which one text extraction does not work.

For each files, a detailed explanation should be done.

- *12032311_table_summary-e.pdf*
  Contents is encrypted helping object 11 0.
  Unfortunately, we do not understand yet how this object can 
  be used to decode content. Any help will be appreciated.
