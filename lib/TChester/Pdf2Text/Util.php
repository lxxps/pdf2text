<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * This class contents usefull function
 *
 * @author Pierrot Evrard
 * @subversion $Id: Util.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Pdf2Text_Util
{
  /**
   * Extract dictionary part from a string
   * 
   * @param string $contents
   * @return string
   * @access public
   * @static
   */
  public static function extractDictionary( $contents )
  {
    // Begin
    $b = strpos( $contents, '<<' , 0 );

    $x = $b + 2;

    $open = 1;
    do
    {
      $bt = strpos( $contents , '<<' , $x );
      $et = strpos( $contents , '>>' , $x );
      if( $et !== false && $bt !== false )
      {
        if( $et < $bt )
        {
          $open--;
          $x = $et + 2;
        }
        else
        {
          $open++;
          $x = $bt + 2;
        }
      }
      elseif( $et !== false )
      {
        $open--;
        $x = $et + 2;
      }
      else
      {
        // An error occur
        break;
      }
    } while( $open > 0 );

    return substr( $contents, $b, $x - $b );
  }

  /**
   * Extract stream part from a string
   *
   * @param string $contents
   * @return string
   * @access public
   * @static
   */
  public static function extractStream( $contents )
  {
    $stream = null;
    // Begin
    $b = strpos( $contents , 'stream' );
    if( $b !== false )
    {
      $b += 6;
      // End
      $e = strpos( $contents, 'endstream', $b );
      // Determine if the object contains an embedded stream object
      if( $b !== false && $e !== false )
      {
        $stream = substr( $contents , $b , $e - $b );
      }
    }

    return $stream;
  }
}
