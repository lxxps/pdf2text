<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class wrapper around an array of key/value pairs
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @abstract
 * @subversion $Id: Accessor.php 6 2010-09-09 13:00:40Z loops $
 */
abstract class TChester_Pdf2Text_Accessor
{
  /**
   * Internal stored is implemented as key/value pairs
   *
   * @var array
   * @access protected
   */
  protected $_data = array();

  /**
   * Default constructor
   *
   * @access public
   * @return void
   */
  public function __construct()
  {
    // Reserved for future use.
  }

  /**
   * Add a key/value pair to the collection, if the
   * key already exists, its value will be overwritten.
   *
   * @param string $name Key to associate with value
   * @param mixed $value Value to store
   * @return void
   * @access public
   */
  public function __set($name, $value)
  {
    $this->_data[$name] = $value;
  }

  /**
   * Retrieves a value for a specified key, if the key
   * does not exist an error will be triggered.
   *
   * @param $name Key to retrieve value for
   * @return mixed Value associated with key or null if error occurred
   * @access public
   */
  public function __get($name)
  {
    if (array_key_exists($name, $this->_data)) {
      return $this->_data[$name];
    }

    return null;
  }

}
