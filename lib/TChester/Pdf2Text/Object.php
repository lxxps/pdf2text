<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage Pdf object
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Object.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Pdf2Text_Object
{
  /**
   * Parent collection
   *
   * @var TChester_Pdf2Text_Objects_Collection
   * @access protected
   */
  protected $_collection;

  /**
   * Object key
   *
   * @var string
   * @access protected
   */
  protected $_key;

  /**
   * Object dictionary
   *
   * @var string
   * @access protected
   */
  protected $_dictionary;

  /**
   * Object contents
   *
   * @var string
   * @access protected
   */
  protected $_contents;

  /**
   * Object extracted contents
   *
   * @var string
   * @access protected
   */
  protected $_readable_contents;

  /**
   * Constuctor
   *
   * @param &TChester_Pdf2Text_Objects_Collection $collection
   * @param string $key
   * @param string $dictionary
   * @param string $contents
   * @return void
   * @access public
   */
  public function __construct( TChester_Pdf2Text_Objects_Collection &$collection , $key , $dictionary , $contents )
  {
    $this->_collection = &$collection;
    $this->_key = $key;
    $this->_dictionary = $dictionary;
    $this->_contents = $contents;
    
    // Automatically add this object to the collection
    $this->_collection->set( $this );
  }

  /**
   * Retrieve object key
   *
   * @param none
   * @return string
   * @access public
   */
  public function getKey()
  {
    return $this->_key;
  }

  /**
   * Retrieve object dictionary
   *
   * @param none
   * @return string
   * @access public
   */
  public function getDictionary()
  {
    return $this->_dictionary;
  }

  /**
   * Retrieve object dictionary
   *
   * @param none
   * @return string
   * @access public
   */
  public function getContents()
  {
    return $this->_contents;
  }

  /**
   * Magic function on string cast, return an empty string
   *
   * @param none
   * @return string
   * @access public
   */
  public function __toString()
  {
    return '';
  }

  /**
   * Functio to tranform current object in another type of object.
   * Type can be Text, Font, CMap, Undefined
   *
   * @param  string $type
   * @param  array  $add_args
   * @return TChester_Pdf2Text_Object
   * @access public
   */
  public function transform( $type , array $add_args = array() )
  {
    $class = 'TChester_Pdf2Text_Object_'.$type;

    if( $this instanceof $class )
    {
      // If the object is already tranformed, return it as is
      return $this;
    }
    else
    {
      if( class_exists( $class , true ) )
      {
        $ref = new ReflectionClass( $class );
        if( $ref->isSubclassOf( 'TChester_Pdf2Text_Object' ) )
        {
          // Using reflection class to create instance 
          // @see http://www.php.net/manual/en/function.call-user-func-array.php#74427
          $args = array( &$this->_collection , $this->_key , $this->_dictionary , $this->_contents );
          foreach( $add_args as $arg )
          {
            array_push( $args , $arg );
          }
          // Note that the new object is automatically assign to the collection
          return $ref->newInstanceArgs( $args );
        }
      }
    }
    // In other case, an error occurs
    throw new InvalidArgumentException( sprintf( 'Unable to find a subclass of TChester_Pdf2Text_Object for type "%s"' , $type ) );
  }

  /**
   * Extract contents to retrieve stream
   *
   * @param none
   * @return string
   * @access public
   */
  public function getReadableContents()
  {
    if( $this->_readable_contents === null )
    {
      $this->_readable_contents = '';

      // Try to get a stream

      // Object does not contain an embedded stream object
      if( $stream = TChester_Pdf2Text_Util::extractStream( $this->_contents ) )
      {
        if( strpos( $this->_dictionary, '/FlateDecode', 0) === false )
        {
          // Stream is plain text
          $this->_readable_contents = $stream;
        }
        else
        {
          // Stream is compressed text using zlib (i.e. FlateDecode)
          $b = 1; // Assume one line stream tag separator
          $e = 1; // Assume one line endstream tag separator
          $len = 0;

          // Check for Carriage Return + Line Feed after stream tag
          if( substr( $stream , 0 , 1 ) == "\r" && substr( $stream , 1 , 1 ) == "\n")
            $b = 2;

          // Check for Carriage Return + Line Feed before endstream tag
          if( substr( $stream , -2 , 1 ) == "\r" && substr( $stream , -1 , 1 ) == "\n" )
            $e = 2;

          // If the length is an indirect object reference that has a format
          // like "/Length # # R" where # # is the key of the referenced
          // object whose contents contain the stream length value. In this
          // case we will just take the shortcut evaluation of the length using
          // stream/endstream positions and removing the leading separators. NOTE:
          // there does not appear to be trailing separators used here so we are
          // ignoring the endpos calculation in the length.
          $matches = array();
          if( preg_match( '~/Length\\s(\\d+\\s\\d+)\\sR~', $this->_dictionary, $matches ) )
          {
            // We can transform this object as Unused
            $this->_collection[$matches[1]]->transform('Unused');
            $len = strlen($stream) - $b;
          }
          else
          {
            // A direct length value is stored in the header and has a format
            // like: "/Length #"
            preg_match( '~/Length\\s(\\d+)~', $this->_dictionary, $matches );
            $len = $matches[1];
          }

          $this->_readable_contents = @ gzuncompress( substr( $stream, $b, $len ) );
        }
      }
      else
      {
        $this->_readable_contents = $this->_contents;
      }
    }
    return $this->_readable_contents;
  }
}
