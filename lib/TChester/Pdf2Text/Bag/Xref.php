<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage Pdf Xref data
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @abstract
 * @subversion $Id: Xref.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Pdf2Text_Bag_Xref extends TChester_Pdf2Text_Bag_Abstract
{
  /**
   * Pattern constant
   *
   * @var string
   * @const
   */
  const PATTERN = '~^(xref)$~';

  /**
   * Constructor
   *
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
    parent::__construct();
    $this->type      = 'xref';
  }

  /**
   * Function to extract Xref informations from current handle
   *
   * @param TChester_Pdf2Text_File_Handle $handle
   * @return boolean
   * @access public
   */
  public function extract( TChester_Pdf2Text_File_Handle $handle )
  {
    $matches = array();
    if( preg_match( self::PATTERN, trim( $handle->getBuffer() ), $matches ) )
    {
      // Is there some stuff to do here?
      return true;
    }
    return false;
  }
  
}
