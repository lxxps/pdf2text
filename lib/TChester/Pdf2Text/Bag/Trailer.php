<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage Pdf trailer data
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Trailer.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Pdf2Text_Bag_Trailer extends TChester_Pdf2Text_Bag_Abstract
{
  /**
   * Pattern constant
   *
   * @var string
   * @const
   */
  const PATTERN = '~^(trailer)$~';

  /**
   * Constructor
   *
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
    parent::__construct();
    $this->type      = 'trailer';
    $this->size      = 0;
    $this->prev      = 0;
    $this->root      = '';
    $this->encrypt   = '';
    $this->info      = '';
    $this->id1       = '';
    $this->id2       = '';
    $this->startXref = 0;
    $this->encrypted = false;
    $this->eof       = '%%EOF';
  }

  /**
   * Function to extract trailer informations from current handle
   *
   * @param TChester_Pdf2Text_File_Handle $handle
   * @return boolean
   * @access public
   */
  public function extract( TChester_Pdf2Text_File_Handle $handle )
  {
    $matches = array();
    if( ( ! $this->seen ) && preg_match( self::PATTERN, trim( $handle->getBuffer() ), $matches ) )
    {
      $contents = $handle->readUntil( '~^('.$this->eof.')$~' );

      $this->dictionary = TChester_Pdf2Text_Util::extractDictionary( $contents );

      // Extract IDs part
      $reg = '~/ID\\s?\\[\\s?<(\\d|\\w+)>\\s?<(\\d|\\w+)>\\s?\\]~';
      if( preg_match( $reg, $this->dictionary, $matches ) )
      {
        $this->id1 = $matches[1];
        $this->id2 = $matches[2];
      }

      // Extract root
      $reg = '~/Root\\s(\\d+\\s\\d+)\\sR~';
      if( preg_match( $reg, $this->dictionary, $matches ) )
        $this->root = $matches[1];

      // Extract info
      $reg = '~/Info\\s(\\d+\\s\\d+)\\sR~';
      if( preg_match( $reg, $this->dictionary, $matches ) )
        $this->info = $matches[1];

      // Extract size
      $reg = '~/Size\\s(\\d+)~';
      if( preg_match( $reg , $this->dictionary, $matches ) )
        $this->size = $matches[1];

      // Extract prev
      $reg = '~/Prev\\s(\\d+)~';
      if( preg_match( $reg , $this->dictionary, $matches ) )
        $this->prev = $matches[1];

      // Extract encrypt
      $reg = '~/Encrypt~';
      if( preg_match( $reg, $contents, $matches ) )
        $this->encrypt = true;

      // Extract startxref
      $reg = '~startxref\\s(\\d+)~';
      if( preg_match( $reg , $contents, $matches ) )
        $this->startXref = $matches[1];

      $this->seen = true;
      return true;
    }
    return false;
  }
  
}
