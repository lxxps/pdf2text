<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage Pdf trailer data
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Body.php 8 2011-09-09 15:00:13Z loops $
 */
class TChester_Pdf2Text_Bag_Body extends TChester_Pdf2Text_Bag_Abstract
{
  /**
   * Pattern constant
   *
   * @var string
   * @const
   */
  const PATTERN = '~^(\\d+ \\d+) obj\\s*(<<.*>>)*(stream)*~';

  /**
   * All objects collection
   *
   * @var TChester_Pdf2Text_Body_Collection_Objects
   * @access protected
   */
  protected $_collection;

  /**
   * Constructor
   *
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
    parent::__construct();
    $this->type      = 'body';
    // Initialize object list
    $this->_collection = new TChester_Pdf2Text_Objects_Collection();
  }

  /**
   * Function to extract objects from current handle
   *
   * @param TChester_Pdf2Text_File_Handle $handle
   * @return boolean
   * @access public
   */
  public function extract( TChester_Pdf2Text_File_Handle $handle )
  {
    $matches = array();
    if( preg_match( self::PATTERN, trim( $handle->getBuffer() ), $matches ) )
    {
      //echo "\nPDF Body\n";
      // At this point, we just save object for future usage
      $key        = $matches[1];
      $contents   = $handle->readUntil( '~^endobj$~' );
      $dictionary = TChester_Pdf2Text_Util::extractDictionary( $contents );
      // Adjust contents extraction to really start at dictionary end position
      $contents   = substr( $contents, strpos( $contents , $dictionary )+strlen( $dictionary ) );

      // Create object
      // Note that the object is automatically add to the collection
      new TChester_Pdf2Text_Object( $this->_collection , $key , $dictionary , $contents );

      return true;
    }
    return false;
  }

  /**
   * Return the current collection
   *
   * @param none
   * @return TChester_Pdf2Text_Body_Collection_Objects
   * @access public
   */
  public function getCollection()
  {
    return $this->_collection;
  }
}
