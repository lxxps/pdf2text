<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage Pdf objects
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Collection.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Pdf2Text_Objects_Collection implements Countable, Iterator, ArrayAccess
{
  /**
   * Array of TChester_Pdf2Text_Body_Object
   * 
   * @var array
   * @access protected
   */
  protected $_objects = array();
  
  /**
   * Add an object to the list
   *
   * @param TChester_Pdf2Text_Object
   * @return TChester_Pdf2Text_Objects_Collection
   * @access public
   */
  public function set( TChester_Pdf2Text_Object $object )
  {
    $this->_objects[$object->getKey()] = $object;
    return $this;
  }

  /**
   * Find font by its name
   *
   * @param string $name
   * @return TChester_Pdf2Text_Object_Font
   * @access public
   */
  public function getFontByName( $name )
  {
    // We must work on a copy directly to not interfer
    // with Iterator interface
    foreach( $this->getObjects() as $object )
    {
      if( $object instanceof TChester_Pdf2Text_Object_Font && $object->getName() === $name )
      {
        return $object;
      }
    }
    return null;
  }


  /**
   * Return list of objects
   *
   * @param
   * @return array
   * @access public
   */
   public function getObjects()
   {
     return $this->_objects;
   }

  /**
   * Supports the Iterator interface
   *
   * @param none
   * @return mixed
   * @access public
   */
  public function current()
  {
    return current( $this->_objects );
  }

  /**
   * Supports the Iterator interface
   *
   * @param none
   * @return mixed
   * @access public
   */
  public function key()
  {
    return key( $this->_objects );
  }

  /**
   * Supports the Iterator interface
   *
   * @param none
   * @return void
   * @access public
   */
  public function next()
  {
    next( $this->_objects );
  }

  /**
   * Supports the Iterator interface
   * 
   * @param none
   * @return void
   * @access public
   */
  public function rewind()
  {
    reset( $this->_objects );
  }

  /**
   * Supports the Iterator interface
   * 
   * @param none
   * @return boolean
   * @access public
   */
  public function valid()
  {
    return current( $this->_objects ) !== false;
  }

  /**
   * Supports the Countable interface
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function count()
  {
    return count( $this->_objects );
  }
  
  /**
   * Supports the ArrayAccess interface.
   * 
   * @param mixed $offset
   * @return boolean
   * @access public
   */
  public function offsetExists( $offset )
  {
    return isset( $this->_objects[$offset] );
  }
  
  /**
   * Supports the ArrayAccess interface.
   * 
   * @param mixed $offset
   * @return mixed
   * @access public
   */
  public function offsetGet( $offset )
  {
    if( $this->offsetExists( $offset ) )
    {
      return $this->_objects[$offset];
    }

    return null;
    // Throw an exception
    throw new InvalidArgumentException( sprintf( 'No object at key "%s"' , $offset ) );
  }
  
  /**
   * Supports the ArrayAccess interface.
   * 
   * This method should not be used.
   * 
   * @param mixed $offset
   * @param mixed $value
   * @return void
   * @access public
   */
  public function offsetSet( $offset , $value )
  {
    // $this->_objects[$offset] = $value;
  }

  /**
   * Supports the ArrayAccess interface.
   * 
   * This method should not be used.
   * 
   * @param mixed $offset
   * @param mixed $value
   * @return void
   * @access public
   */
  public function offsetUnset( $offset )
  {
    // unset($this->_objects[$offset]);
  }
  
}
