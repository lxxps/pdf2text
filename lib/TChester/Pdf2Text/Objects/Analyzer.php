<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage object collection and dispatch them as Font, Cmap, Text, etc.
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @abstract
 * @subversion $Id: Analyzer.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Pdf2Text_Objects_Analyzer
{
  /**
   * Current collection
   *
   * @var TChester_Pdf2Text_Objects_Collection
   * @access protected
   */
  protected $_collection;

  /**
   * Constructor
   *
   * @param &TChester_Pdf2Text_Objects_Collection $collection
   * @access public
   */
  public function __construct( TChester_Pdf2Text_Objects_Collection &$collection )
  {
    $this->_collection = &$collection;
    $this->_analyze();
  }

  /**
   * Analyze object from their dictionnary and them contents
   *
   * @param none
   * @return void
   * @access protected
   */
  protected function _analyze()
  {
    // TODO find text other several objects (special MS Word stuff, thanks MS)

//    $q = 0;
//    $qmax = 10;

    // First step, detect ObjStm to unpack them
    // We must work on keys because objects can change in runtime
    foreach( array_keys( $this->_collection->getObjects() ) as $key )
    {
      // Direct access
      $object = $this->_collection[$key];

      if( strpos( $object->getDictionary() , '/ObjStm' ) !== false )
      {
        // Transform object
        $object->transform( 'ObjStm' );
      }
    }

    // Second step, final analyse
    
    // We must work on keys because objects can change in runtime
    foreach( array_keys( $this->_collection->getObjects() ) as $key )
    {
//      if( $q > $qmax ) { continue; }
//      $q++;

      // Do not used object but direct access
      $object = $this->_collection[$key];
      
      if( strpos( $object->getDictionary() , '/Contents' ) !== false )
      {
        $object->transform( 'Contents' );
      }

      // Do not used object but direct access
      $object = $this->_collection[$key];

      if( get_class( $object ) === 'TChester_Pdf2Text_Object' )
      {
        $object->transform( 'Unused' );
      }
    }
  }

//  /**
//   * Return true if the object is a text object
//   *
//   * @param TChester_Pdf_Object $object
//   * @return boolean
//   * @access protected
//   */
//  protected function _isText( TChester_Pdf2Text_Object $object )
//  {
//    $dictionary = $object->getDictionary();
//
//    // Non-text stream objects such as images
//    if( strpos( $dictionary, '/Device' ) !== false ||
//        strpos( $dictionary, '/Image' ) !== false ||
//        strpos( $dictionary, '/Metadata' ) !== false ||
//        strpos( $dictionary, '/ColorSpace' ) !== false ||
//        strpos( $dictionary, '/Encoding' ) !== false )
//    {
//      return false; // '** Device|Image|Metadata **'
//    }
//
//    // Encodings we are not able to parse yet
//    if( strpos( $dictionary, '/ASCIIHexDecode' ) !== false ||
//        strpos( $dictionary, '/ASCII85Decode' ) !== false ||
//        strpos( $dictionary, '/LZWDecode' ) !== false ||
//        strpos( $dictionary, '/RunLengthDecode' ) !== false ||
//        strpos( $dictionary, '/CCITTFaxDecode' ) !== false ||
//        strpos( $dictionary, '/DCTDecode' ) !== false)
//    {
//      return false; // '** Unhandled Encoding **'
//    }
//
//    $matches = array();
//
//    // Filter out PDF hint tables
//    if( preg_match('~/[LS]\\s\\d+~', $dictionary, $matches ) )
//    {
//      return false; //'** HINT TABLE **';
//    }
//
//    // Filter out font program information
//    if( preg_match('~/Length[123]\\s\\d+~', $dictionary, $matches ) )
//    {
//      return false; //'** FONT PROGRAM **';
//    }
//
//    // Filter out font program information
//    if( preg_match('~/N\\s~', $dictionary, $matches ) )
//    {
//      return false; //'** FONT PROGRAM **';
//    }
//
//    // To match text object we need to find some BT or ET tag
//    if( ! preg_match( '~(\\bBT\\s)|(\\sET\\b)~s' , $object->getReadableContents() ) )
//    {
//      return false;
//    }
//
//    return true;
//  }
  
}
