<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Interface describing the types of data and metadata that
 * are available from the pdf2text object.
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @interface
 * @subversion $Id: Interface.php 6 2010-09-09 13:00:40Z loops $
 */
interface TChester_Pdf2Text_MetaInfo_Interface
{
  /**
   * The document's title.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getTitle();

  /**
   * The name of the person who created the document.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getAuthor();

  /**
   * The subject of the document.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getSubject();

  /**
   * Keywords associated with the document.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getKeywords();

  /**
   * The name of the application that originally created the document
   * before it was converted to PDF format.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getCreator();

  /**
   * The name of the application that converted the original document
   * into PDF format.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getProducer();

  /**
   * The human-readable date and time when the PDF was created.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getCreationDate();

  /**
   * The human-readable date and time of the most recent modification.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getModDate();

  /**
   * The textual contents of the PDF file.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getContents();
}