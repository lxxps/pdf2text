<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage font, especially when they have an associated CMap table
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @author Suriya Kaewmungmuang <sc450617@hotmail.com>
 * @subversion $Id: Font.php 10 2011-09-19 13:00:36Z loops $
 */
class TChester_Pdf2Text_Object_Font extends TChester_Pdf2Text_Object
{
  /**
   * Encoding
   *
   * @var string
   * @access encoding
   */
  protected $_encoding = 'StandardEncoding';

  /**
   * Decoder object
   *
   * @var TChester_Pdf2Text_Decoder_Interface
   * @access encoding
   */
  protected $_decoder;
  
  /**
   * CMap object, if any
   *
   * @var TChester_Pdf2Text_Object_CMap
   * @access protected
   */
  protected $_cmap = null;

  /**
   * Constuctor
   *
   * @param &TChester_Pdf2Text_Objects_Collection $collection
   * @param string $key
   * @param string $dictionary
   * @param string $contents
   * @param string $name
   * @access public
   */
  public function __construct( TChester_Pdf2Text_Objects_Collection &$collection , $key , $dictionary , $contents )
  {
    parent::__construct( $collection , $key , $dictionary , $contents );

    // Detect encoding, note that encoding is required for a font dictionary
    $matches = array();
    if( preg_match( '~/Encoding\\s?/([-\\w]+)~' , $this->_dictionary , $matches ) )
    {
      $this->_encoding = $matches[1];
    }
    // Encoding can be another object
    elseif( preg_match( '~/Encoding\\s([0-9]+\\s[0-9]+)\\sR~' , $this->_dictionary , $matches ) )
    {
      $this->_encoding = $this->_collection[$matches[1]]->transform( 'Encoding' )->getEncoding();
    }

    // Load decoder
    $this->_loadDecoder();

    // Detect CMap
    // Dictionary may contents an ToUnicode CMap reference
    if( preg_match( '~/ToUnicode\\s(\\d+\\s\\d+)\\sR~' , $this->_dictionary , $matches ) )
    {
      // Transform to CMap object
      $this->_cmap = $this->_collection[$matches[1]]->transform( 'CMap' );
    }

//    print '<pre>';
//    print 'Font key '.$this->_key."\n";
//    print 'With CMap '.( (int)(bool)$this->_cmap )."\n";
//    print 'CMap Key '.( $this->_cmap ? $this->_cmap->getKey() : 'NONE' )."\n";
//    print 'Encoding '.( $this->_encoding ? $this->_encoding : 'NONE' )."\n";
//    print '</pre>';
  }

  /**
   * Return the font name
   *
   * @param none
   * @return void
   * @access protected
   */
  protected function _loadDecoder()
  {
    $class = 'TChester_Pdf2Text_Decoder_'.$this->_encoding;

    if( class_exists( $class , true ) )
    {
      $ref = new ReflectionClass( $class );
      if( $ref->implementsInterface( 'TChester_Pdf2Text_Decoder_Interface' ) )
      {
        $this->_decoder = call_user_func( array( $class , 'getInstance' ) );
      }
      else
      {
        throw new LogicException( sprintf( 'Class %s must implements TChester_Pdf2Text_Decoder_Interface interface' , $class ) );
      }
    }
  }

  /**
   * Return the font encoding
   *
   * @param none
   * @return string
   * @access public
   */
  public function getEncoding()
  {
    return $this->_encoding;
  }

  /**
   * Return true if the fiont has an associated CMap
   *
   * @param none
   * @return boolean
   * @access public
   */
  public function hasCMap()
  {
    return (bool)$this->_cmap;
  }

  /**
   * Function to transform string helping font data
   *
   * @param string $str
   * @return string
   * @access public
   */
  public function __invoke( $str )
  {
    $change = false;
    
    $len = mb_strlen( $str );
    
    // At first apply CMap
    // We must restrict this check to at least 2-digits string containing only hexa character
    if( $this->_cmap instanceof TChester_Pdf2Text_Object_CMap && $len > 1 && $len % 2 === 0 && ( ! preg_match( '~[^0-9A-F]~u' , $str ) ) )
    {
      // In this case, we should not have anything else than mapped characters
      $str = $this->_cmap->__invoke( $str );
      $change = true;
    }

    // Then apply decoder
    if( $this->_decoder instanceof TChester_Pdf2Text_Decoder_Interface && $this->_decoder->detect( $str ) )
    {
      // Apply decoder
      $str = $this->_decoder->__invoke( $str );

      // Apply the CMap to extra unconverted character
      if( $this->_cmap instanceof TChester_Pdf2Text_Object_CMap )
      {
        $str = preg_replace_callback( '~\\\\[0-7]{3}~' , array( $this , '_extra' ) , $str );
      }
      $change = true;
    }

    // If not any decoder and text is just a single character string
    if( $this->_cmap instanceof TChester_Pdf2Text_Object_CMap && $len === 1 )
    {
      // Convert this character to hexadecimal unicode
      $tmp = iconv( 'UTF-8' , 'UTF-16//TRANSLIT//IGNORE' , $str );
      try
      {
        // Skip the first two bits
        $str = $this->_cmap->__invoke( sprintf( '%02X' , ord( $tmp{2} ) ).sprintf( '%02X' , ord( $tmp{3} ) ) );
        $change = true;
      }
      catch( UnexpectedValueException $e )
      {
        // Do nothing
      }
    }

    if( $change )
    {
      return $str;
    }
    else
    {
      return iconv( 'ISO-8859-1' , 'UTF-8//TRANSLIT//IGNORE' , $str );
    }
  }

  /**
   * Internal conversion for extra octal character using the CMap.
   *
   * @param array $matches
   * @return string
   * @access protected
   */
  protected function _extra( array $matches )
  {
    // We need to check two type of replacements, case where the CMap
    // contains 2-digits character, and case where it contains 4 digits character
    $code = sprintf( '%04X' , octdec( substr( $matches[0] , 1 ) ) );
    
    $tmp = '';
    
    if( substr( $code , 0 , 2 ) === '00' )
    {
      // We want to check 2-digits, at first
      $tmp = $this->_cmap->__invoke( substr( $code , 2 , 2 ) );
      // Note that we expect a special character, so if it looks like the same
      // it means that the convertion has failed
      if( $tmp === substr( $code , 2 , 2 ) ) $tmp = '';
    }
    
    if( $tmp === '' )
    {
      // We have failed with the two digits, attemp a second run with 4-digits
      $tmp = $this->_cmap->__invoke( $code );
    }
    
    return $tmp;
  }
}
