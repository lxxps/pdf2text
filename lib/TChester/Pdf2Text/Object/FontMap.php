<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage font, especially when they have an associated CMap table
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @author Suriya Kaewmungmuang <sc450617@hotmail.com>
 * @subversion $Id: FontMap.php 8 2011-09-09 15:00:13Z loops $
 */
class TChester_Pdf2Text_Object_FontMap extends TChester_Pdf2Text_Object_Ressources
{
  /**
   * Parse the dictionnary to provide a font map.
   *
   * @param none
   * @return void
   * @access protected
   */
  protected function _createFontMap()
  {
    // We need to deal with dictionary like 
    // <</F1 20 0 R/F2 30 0 R/F3 40 0 R/F4 25 0 R/F5 35 0 R >>
    // where all objects are fonts
    if( preg_match_all( '~/([-.\\w\\d]+)\\s(\\d+\\s\\d+)\\sR~i' , $this->_dictionary , $matches , PREG_SET_ORDER ) )
    {
      for( $i = 0, $imax = count( $matches ); $i < $imax; $i++ )
      {
        if( isset( $this->_collection[$matches[$i][2]] ) )
        {
          $this->_collection[$matches[$i][2]]->transform( 'Font' );
          $this->_font_map[$matches[$i][1]] = $matches[$i][2];
        }
      }
    }
  }
  
}
