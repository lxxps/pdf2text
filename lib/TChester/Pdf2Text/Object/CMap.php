<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage CMap table
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @author Suriya Kaewmungmuang <sc450617@hotmail.com>
 * @subversion $Id: CMap.php 13 2012-06-15 09:38:44Z loops $
 */
class TChester_Pdf2Text_Object_CMap extends TChester_Pdf2Text_Object
{

  /**
   * Character table
   *
   * @var array
   * @access protected
   */
  protected $_table = array();

  /**
   * Constuctor
   *
   * @param &TChester_Pdf2Text_Objects_Collection $collection
   * @param string $key
   * @param string $dictionary
   * @param string $contents
   * @access public
   */
  public function __construct( TChester_Pdf2Text_Objects_Collection &$collection , $key , $dictionary , $contents )
  {
    parent::__construct( $collection , $key , $dictionary , $contents );

    // Create character map
    $this->_extract();

//    print '<pre>';
//    print 'Cmap '.$this->_key."\n";
//    print 'Dictionnary : '.htmlspecialchars($this->_dictionary)."\n";
//    print 'Table : '."\n".var_export( $this->_table , 1 )."\n";
////    print 'Extracted from : '."\n".htmlspecialchars( $this->_readable_contents )."\n";
//    print '</pre>';
  }

  /**
   * Extract CMap data.
   *
   * CMap contains severals informations.
   *
   * begincodespacerange and endcodespacerange, contains one or more lines like
   * this:
   *  - <A0> <FC>
   *  - <E040> <FCFC>
   * For now, we don't really know what to do with that.
   *
   * beginbfchar and endbfchar, contains one or more lines like
   * this:
   *  - <A0> <00DF>
   *  - <E040> <FCFC>
   * Represents source code to destination code.
   * 
   * beginbfrange and endfrange, contains one or more lines like
   * this:
   *  - <00> <5E> <20>
   *  - <0000> <005E> <0020>
   *  - <005F> <0061> [<00660066> <00660069> <00660066006C>]
   * Two first representations correspond to a range of code that start
   * from the destination code (in second case 0000 becomes 0020, 0001 becomes
   * 0021, 0002 becomes 0022, etc).
   * Third representations means that each code in the range have is own
   * destination in square brackets (005F becomes 00660066, 0060 becomes
   * 00660069 and 0061 becomes 00660066006C).
   *
   *
   * @param none
   * @return void
   * @access protected
   */
  protected function _extract()
  {
    // begincodespacerange match (required)
//    $matches = array();
//    preg_match( '~begincodespacerange\\s+<(([\\dA-F]{2}){1,2})>\\s?<(([\\dA-F]{2}){1,2})>\\s+endcodespacerange~i' , $this->getReadableContents() , $matches );
    
    $str = $this->getReadableContents();
    $x = strpos( $str , 'endcodespacerange' ) + strlen('endcodespacerange');
    $xmax = strlen( $str );

    // Loop until no definition is available
    do
    {
      // Let's try to find the first definition beeween beginbfchar and beginbfrange
      $bfc = strpos( $str , 'beginbfchar' , $x );
      $bfr = strpos( $str , 'beginbfrange' , $x );
      
      if( $bfc !== false && $bfr !== false )
      {
        if( $bfc < $bfr )
        {
          $this->_extractBfChar( substr( $str , $bfc + strlen('beginbfchar') , strpos( $str , 'endbfchar' , $bfc + strlen('beginbfchar') ) - $bfc - strlen('beginbfchar') ) );
          $x = strpos( $str , 'endbfchar' , $bfc + strlen('beginbfchar') ) + strlen('endbfchar');
        }
        else
        {
          $this->_extractBfRange( substr( $str , $bfr + strlen('beginbfrange') , strpos( $str , 'endbfrange' , $bfr + strlen('beginbfrange') ) - $bfr - strlen('beginbfrange') ) );
          $x = strpos( $str , 'endbfrange' , $bfr + strlen('beginbfrange') ) + strlen('endbfrange');
        }
      }
      elseif( $bfc !== false )
      {
        $this->_extractBfChar( trim( substr( $str , $bfc + strlen('beginbfchar') , strpos( $str , 'endbfchar' , $bfc + strlen('beginbfchar') ) - $bfc - strlen('beginbfchar') ) ) );
        $x = strpos( $str , 'endbfchar' , $bfc + strlen('beginbfchar') ) + strlen('endbfchar');
      }
      elseif( $bfr !== false )
      {
        $this->_extractBfRange( substr( $str , $bfr + strlen('beginbfrange') , strpos( $str , 'endbfrange' , $bfr + strlen('beginbfrange') ) - $bfr - strlen('beginbfrange') ) );
        $x = strpos( $str , 'endbfrange' , $bfr + strlen('beginbfrange') ) + strlen('endbfrange');
      }
    } while( $bfc !== false && $bfr !== false );
  }

  /**
   * Extract BfChar informations from a string
   *
   * beginbfchar and endbfchar, contains one or more lines like
   * this:
   *  - <A0> <00DF>
   *  - <E040> <FCFC>
   * Represents source code to destination code.
   *
   * @param string $str
   * @return void
   * @access protected
   */
  protected function _extractBfChar( $str )
  {
    $matches = array();
    if( preg_match_all( '~<(([\\dA-F]{2}){1,2})>\\s?<((([\\dA-F]{2}){1,2})+)>~si' , $str , $matches , PREG_SET_ORDER ) )
    {
      for( $j = 0, $jmax = count($matches); $j < $jmax; $j++ )
      {
        // We need to take care of key that can looks like <01> (2-digits) or <0001> (4-digits)
        $this->_table[sprintf( '%0'.strlen($matches[$j][1]).'X' , hexdec( $matches[$j][1] ) )] = $this->_translate( $matches[$j][3] );
      }
    }
  }

  /**
   * Extract BfRange informations
   *
   * beginbfrange and endfrange, contains one or more lines like
   * this:
   *  - <00> <5E> <0020>
   *  - <0000> <005E> <0020>
   *  - <005F> <0061> [<00660066> <00660069> <00660066006C>]
   * Two first representations correspond to a range of code that start
   * from the destination code (in second case 0000 becomes 0020, 0001 becomes
   * 0021, 0002 becomes 0022, etc).
   * Third representations means that each code in the range have is own
   * destination in square brackets (005F becomes 00660066, 0060 becomes
   * 00660069 and 0061 becomes 00660066006C).
   * 
   * @param string $str
   * @return void
   * @access protected
   */
  protected function _extractBfRange( $str )
  {
    $range = explode( "\n" , $str );

    for( $j = 0, $jmax = count( $range ); $j < $jmax; $j++ )
    {
      $matches = array();
      // Offset range
      if( preg_match( '~<(([\\dA-F]{2}){1,2})>\\s?<(([\\dA-F]{2}){1,2})>\\s?<([\\dA-F]{4})>~i' , $range[$j] , $matches ) )
      {
        $min = hexdec( $matches[1] );
        $max = hexdec( $matches[3] );
        $offset = hexdec( $matches[5] );

        for( $k = 0, $kmax = $max - $min; $k <= $max; $k++ )
        {
          $this->_table[ sprintf( '%04X' , $k + $min ) ] = $this->_translate( sprintf( '%04X' , $k + $offset ) );
        }
      }

      // Special range
      if( preg_match( '~<(([\\dA-F]{2}){1,2})>\\s?<(([\\dA-F]{2}){1,2})>\\s?\\[((<([\\dA-F]{4})>\\s?)+)\\]~i' , $range[$j] , $matches ) )
      {
        $min = hexdec( $matches[1] );
        $max = hexdec( $matches[3] );
        $values = array_map( 'trim' , explode( ' ' , $matches[5] ) );

        // Reformat values
        for( $k = 0, $kmax = count( $values ); $k < $kmax; $k++ )
        {
          // Remove first and last char from $values
          $values[$k] = $this->_translate( substr( $values[$k] , 1 , -1 ) );
        }
        // Append values to table
        for( $k = 0, $kmax = $max - $min; $k <= $kmax; $k++ )
        {
          $this->_table[ sprintf( '%04X' , $k + $min ) ] = $values[ $k ];
        }
      }
    }
  }

  /**
   * Function to translate UTF-16 hexadecimal code in UTF-8
   *
   * Call a special decoder to escape strange character.
   *
   * @param string $hex
   * @return string
   * @access protected
   */
  protected function _translate( $hex )
  {
    return TChester_Pdf2Text_Decoder_HexaUnicode::getInstance()->__invoke( $hex );
  }

  /**
   * Function to transform string helping character table
   * Note that we do not consider unmapped character.
   *
   * @param string $str
   * @return string
   * @access public
   *
   * @throws UnexpectedValueException
   */
  public function __invoke( $str )
  {
    $tmp = '';
    for( $i = 0, $imax = strlen( $str ); $i < $imax; $i += 2 )
    {
      
      // At first, we look for 2-digits character
      if( isset($this->_table[substr( $str , $i , 2 )]) )
      {
        $tmp .= $this->_table[substr( $str , $i , 2 )];
        continue;
      }
      
      // If we did not found a 2-digits one, look for a for digits, if possible
      if( $i+2 < $imax )
      {
        if( isset($this->_table[substr( $str , $i , 4 )]) )
        {
          $tmp .= $this->_table[substr( $str , $i , 4 )];
          // Increase
          $i += 2;
          continue;
        }
        if( substr( $str , $i , 2 ) === '00' )
        {
          // We allow this case, and only this case
          $this->_table[substr( $str , $i , 4 )] = TChester_Pdf2Text_Decoder_HexaUnicode::getInstance()->__invoke( substr( $str , $i , 4 ) );
          $tmp .= $this->_table[substr( $str , $i , 4 )];
          // Increase
          $i += 2;
          continue;
        }
        
        //trigger_error( sprintf( 'Unmapped character "%s" or "%s"' , substr( $str , $i , 2 ) , substr( $str , $i , 4 ) ) , E_USER_WARNING );
        // throw new UnexpectedValueException( sprintf( 'Unmapped character "%s" or "%s"' , substr( $str , $i , 2 ) , substr( $str , $i , 4 ) ) );
      }
      else
      {
        // trigger_error( sprintf( 'Unmapped character "%s"' , substr( $str , $i , 2 ) ) , E_USER_WARNING );
        // throw new UnexpectedValueException( sprintf( 'Unmapped character "%s"' , substr( $str , $i , 2 ) ) );
      }
      
      // We may want to keep it like that
      // We have some cases when the text is like "10", so detected as hexadecimal, but not in our table
      // Note that we want to apply a conversion on the text, just to be sure that it will be in UTF-8
      $tmp .= iconv( 'ISO-8859-1' , 'UTF-8//TRANSLIT//IGNORE' , substr( $str , $i , 2 ) );
      
    }
    return $tmp;
  }
}
