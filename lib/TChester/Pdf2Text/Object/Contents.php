<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage contents Pdf object
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @author Suriya Kaewmungmuang <sc450617@hotmail.com>
 * @subversion $Id: Contents.php 8 2011-09-09 15:00:13Z loops $
 */
class TChester_Pdf2Text_Object_Contents extends TChester_Pdf2Text_Object_Ressources
{
  /**
   * Text map, array of array like this structure
   * 
   * array(
   *   array(
   *     'font' => font name, if any
   *     'text' => text representation, need conversion
   *     'newline' => new line flag
   *   ) ,
   *   array(
   *     'font' => font name, if any
   *     'text' => text representation, need conversion
   *     'newline' => new line flag
   *   ) ,
   *   array(
   *     'font' => font name, if any
   *     'text' => text representation, need conversion
   *     'newline' => new line flag
   *   ) ,
   * )
   *
   * @var array
   * @access protected
   */
  protected $_text_map = array();

  /**
   * Original text string
   *
   * @var string
   * @access protected
   */
  protected $_text_str;

  /**
   * Final text (after transformation)
   *
   * @var string
   * @access protected
   */
  protected $_text;

  /**
   * Text flag
   *
   * @var boolean
   * @access protected
   */
  protected $_has_text = false;

  /**
   * Constuctor
   *
   * @param &TChester_Pdf2Text_Objects_Collection $collection
   * @param string $key
   * @param string $dictionary
   * @param string $contents
   * @access public
   */
  public function __construct( TChester_Pdf2Text_Objects_Collection &$collection , $key , $dictionary , $contents )
  {
    parent::__construct( $collection , $key , $dictionary , $contents );

    // Try to found ressources
    if( preg_match( '~/Resources\\s?(\\d+\\s\\d+)\\sR~' , $this->getDictionary() , $matches ) )
    {
      // Resources is a reference to another dictionary
      // Transform object as Ressources
      if( isset($this->_collection[$matches[1]]) )
      {
        $object = $this->_collection[$matches[1]]->transform( 'Ressources' );
        // Assign font map
        $this->_font_map = $object->getFontMap();
      }
    }
    else
    {
      // Create font map
      $this->_createFontMap();
    }

    // If the font map is not empty, this contents can contain text
    if( $this->_font_map )
    {
      // Determine single object content
      if( preg_match( '~/Contents\\s?(\\d+\\s\\d+)\\sR~' , $this->getDictionary() , $matches ) )
      {
        // Contents is a reference to another dictionary
        // Transform object as Text
        if( isset($this->_collection[$matches[1]]) )
        {
          $object = $this->_collection[$matches[1]]->transform( 'Text' );
          // Assign text
          $this->_text_str = $object->getReadableContents();
          $this->_has_text = true;
        }
      }
      elseif( preg_match( '~/Contents\\s?\\[\\s?((\\d+\\s\\d+\\sR\\s?)+)\\s?\\]~' , $this->getDictionary() , $matches ) )
      {
        // Match 1 contains a list of object reference
        $tmp = array_filter( array_map( 'trim' , explode( 'R' , $matches[1] ) ) );
        foreach( $tmp as $key )
        {
          if( isset($this->_collection[$key]) )
          {
            $object = $this->_collection[$key]->transform( 'Text' );
            // Assign text
            $this->_text_str .= $object->getReadableContents();
            $this->_has_text = true;
          }
        }
      }
    }

    if( $this->_has_text )
    {
      // Create text map
      $this->_createTextMap();
    }


//    print '<pre>';
//    print 'Contents key '.$this->_key."\n";
//    print 'Dictionary: '.htmlspecialchars( $this->_dictionary )."\n";
//    print 'Has text: '.var_export( $this->_has_text , true )."\n";
//    if( $this->_has_text )
//    {
//      print 'Font map:'."\n".htmlspecialchars( var_export( $this->_font_map , true ) )."\n";
//      print 'Text map:'."\n".htmlspecialchars( var_export( $this->_text_map , true ) )."\n";
////      print 'Extracted from : '."\n".htmlspecialchars( $this->_text_str )."\n";
//      print 'Text : '."\n".htmlspecialchars( $this->getText() )."\n";
//    }
//    print '</pre>';
    
  }

  /**
   * Check if this object has text.
   *
   * @param none
   * @return boolean
   * @access protected
   */
  public function hasText()
  {
    return $this->_has_text;
  }

  /**
   * Return font object by its name
   *
   * @param string $name
   * @return TChester_Pdf2Text_Object_Font
   * @access protected
   */
  protected function _getFontByName( $name )
  {
    if( isset( $this->_font_map[$name] ) && isset($this->_collection[$this->_font_map[$name]]) )
    {
      return $this->_collection[$this->_font_map[$name]];
    }
  }
  
  /**
   * Parse the contents to provide a text map.
   *
   * Text content start with BT and ending with ET.
   *
   * Or
   * 
   * Text object start with BT and ending with ET.
   * Text contents start with BDC and ending with EWC.
   *
   * Or
   *
   * Text object start with BDC and ending with EWC.
   * Text contents start with BT and ending with ET.
   *
   * For convenience, we focus only on the first case.
   *
   * Text definition appears before Tj (or TJ) with severals format:
   *  - [(Text)12(Text)]
   *  - (Text)
   *  - [<00AA05EF>-5<0AF787F>]
   *  - <00AA05EF>
   *
   * Font can be found before Tf tag.
   *
   * Font persist beetween each Tj.
   * 
   * Try to determine if a new line appears before the text.
   *
   * @param none
   * @return void
   * @access protected
   */
  protected function _createTextMap()
  {
    $str = $this->_text_str;

    // Et and BT can appears beetween () but not beetween <> 
    // because of hexadecimal representation
    
    // Remove all BT in parenthese by #~1~#
    while( preg_match( '~(\\(([^\(\\)]|(\\\\[\\(\\)]))*)BT(([^\\(\\)]|(\\\\[\\(\\)])*(?<!\\\\)\\)))~s' , $str , $matches ) )
    {
      $str = preg_replace( '~(\\(([^\(\\)]|(\\\\[\\(\\)]))*)BT(([^\\(\\)]|(\\\\[\\(\\)])*(?<!\\\\)\\)))~s' , '${1}#~1~#${4}' , $str );
    }
    // Remove all ET in parenthese by #~2~#
    while( preg_match( '~(\\(([^\(\\)]|(\\\\[\\(\\)]))*)ET(([^\\(\\)]|(\\\\[\\(\\)])*(?<!\\\\)\\)))~s' , $str , $matches ) )
    {
      $str = preg_replace( '~(\\(([^\(\\)]|(\\\\[\\(\\)]))*)ET(([^\\(\\)]|(\\\\[\\(\\)])*(?<!\\\\)\\)))~s' , '${1}#~2~#${4}' , $str );
    }

    // Prepare replacements
    $replace = array( '#~1~#' => 'BT' , '#~2~#' => 'ET' );

    $bt = strpos( $str , 'BT' );

    // Initialize font name
    $font = null;

    // Initialize new line
    $newline = false;

    // Current Y position (used to determine new line)
    $y = 0;

    // Loop other all text begin tag
    while( $bt !== false )
    {
      $x = $lx = $bt + 2;

      // Next ET
      $et = strpos( $str , 'ET' , $x );

      if( $et === false )
      {
        break;
      }

      // Loop beetween BT and ET
      while( true )
      {
        // Try to find another Tj
        $tj1 = strpos( $str , 'Tj' , $x );
        $tj2 = strpos( $str , 'TJ' , $x );
        $tj = $tj1 !== false && $tj2 !== false ? min( $tj1, $tj2 ) : ( $tj1 !== false ? $tj1 : $tj2 );

        if( $tj === false )
        {
          // Not any other text
          break;
        }

        if( $et !== false && $tj > $et )
        {
          // This TJ does not correspond to this block
          break;
        }

        // Try to find a font name
        $tf = strpos( $str , 'Tf' , $x );

        if( $tf !== false && $tf < $tj )
        {
          // We have a new font name before text
          $font_part = substr( $str , $x , $tf - $x );
          if( preg_match( '~\\s/([-.\\w\\d]+)\\s+[-.\\d]+\\s$~' , substr( $str , $x , $tf - $x ) , $matches ) )
          {
            // We have found a font name
            $font = $matches[1];
          }
        }
        else
        {
          // In other case, the font persist
          $tf = 0;
        }

        // Try to find a Y position
        $tm = strpos( $str , 'Tm' , $x );

        if( $tm !== false && $tm < $tj )
        {
          // We have a new font name before text
          if( preg_match( '~\\s([.\\d]+)\\s$~' , substr( $str , $x , $tm - $x ) , $matches ) )
          {
            // We have found a y position
            if( $matches[1] !== $y )
            {
              if( $y !== 0 ) $newline = true;
              $y = $matches[1];
            }
          }
        }
        else
        {
          // In other case, the y position persists
          $tm = 0;
        }

        // Adjust offset
        if( max( $tf , $tm ) > 0 ) $x = max( $tf , $tm ) + 2;

        // Reach next begin of text representation
        // Loop to escape special stuff
        do
        {
          // Try to find begin of text representation
          $next_1 = strpos( $str , '(' , $x );
          $next_2 = strpos( $str , '[' , $x );
          $next_3 = strpos( $str , '<' , $x );

          // Additional check on next_3
          $matches = array();

          if( $next_1 !== false || $next_2 !== false || $next_3 !== false )
          {
            if( $next_1 === false ) $next_1 = PHP_INT_MAX;
            if( $next_2 === false ) $next_2 = PHP_INT_MAX;
            if( $next_3 === false ) $next_3 = PHP_INT_MAX;
            $x = min( $next_1 , $next_2 , $next_3 ) - 1;
          }

          // For now, we just escape '<' found if it belong to BDC definition
          // We need to deal with stuff like "/Span<</ActualText<FEFF0045>>> BDC
          if( $x + 1 === $next_3 && preg_match( '~^<<([^<>]*(<[^<>]*>)*)*>>\\s?BDC~' , substr( $str , $next_3 ) , $matches ) )
          {
            $x = $next_3 + strlen( $matches[0] );
            $next_1 = strpos( $str , '(' , $x );
            $next_2 = strpos( $str , '[' , $x );
            $next_3 = strpos( $str , '<' , $x );
          }
          else
          {
            break;
          }

        } while( $next_1 !== false || $next_2 !== false || $next_3 !== false );

        $map = array(
          'font' => $font ,
          // Restore ET and BT in ()
          'text' => trim( strtr( substr( $str , $x , $tj - $x ) , $replace ) ) ,
          // Is there any T*, Td, TD before the text representation
          'newline' => $newline || preg_match( '~(((?<!\\s0\\s)T[Dd])|(T\\*))~' , substr( $str , $lx , $x - $lx ) ) ,
        );

        if( preg_match( '~\\s-[0-9\.]+\\s0\\sT[Dd]~' , substr( $str , $lx , $x - $lx ) ) )
        {
          // This text is X positionned
          // Determine where to insert
          for( $d = 1, $max = count($this->_text_map); $max - $d >= 0; $d++ )
            if( $this->_text_map[$max - $d]['newline'] ) break;
          // This part sould be placed before the last one (-x position)
          array_splice( $this->_text_map , count($this->_text_map) - $d , 0 , array( $map ) );
          // Switch newline status
          $tmp = $this->_text_map[count($this->_text_map) - $d - 1]['newline'];
          $this->_text_map[count($this->_text_map) - $d - 1]['newline'] = $this->_text_map[count($this->_text_map) - $d]['newline'];
          $this->_text_map[count($this->_text_map) - $d]['newline'] = $tmp;
        }
        else
        {
          array_push( $this->_text_map , $map );
        }

        // Move offset
        $x = $lx = $tj + 2;
        // Reset newline
        $newline = false;
      }

      // Find next BT
      if( $et !== false )
      {
        $bt = strpos( $str , 'BT' , $et + 2 );
      }
      else
      {
        $bt = strpos( $str , 'BT' , $x );
      }

    }

  }

  /**
   * Goes other text map to create text.
   *
   * Text map looks like an array where each elements looks like this:
   *
   * array(
   *   'font' => 'Font name' ,
   *   'text' => 'Text representation' ,
   *   'newline' => true or false
   * )
   *
   * Text representation can be several formats:
   *  - [(Text)12(Text)]
   *  - (Text)
   *  - [<00AA05EF>]12[<0AF787F>]
   *  - <00AA05EF>
   *
   * @return string
   * @access public
   */
  public function getText()
  {
    if( $this->_text === null )
    {
      $this->_text = '';

      // Transform the text
      foreach( $this->_text_map as $map_key => $info )
      {
        $matches = array();
        $data = '';
        if( strpos( $info['text'] , '[<' ) !== false )
        {
          // Case where text look like
          // [<0036002900340032>19<0039000D003300350032000D003300250029002E0025>]
          // That means that the font should have a CMap
          if( preg_match_all( '~<([\\dA-F]+)>~' , $info['text'] , $matches ) )
          {
            for( $i = 0, $imax = count($matches[1]); $i < $imax; $i++ )
            {
              $data .= $matches[1][$i];
            }
          }
        }
        elseif( strpos( $info['text'] , '[(' ) !== false )
        {
          // Case where text look like
          // [(I LOV)4(E ME)3(AT)]
          // Replace escaped parentheses
          $replace = array( '\\(' => '#~1~#' , '\\)' => '#~2~#' );
          if( preg_match_all( '~\\(([^\\)]+)\\)~' , strtr( $info['text'] , $replace ) , $matches ) )
          {
            for( $i = 0, $imax = count($matches[1]); $i < $imax; $i++ )
            {
              $data .= $matches[1][$i];
            }
          }
          // Restore escaped parentheses
          $replace = array( '#~1~#' => '(' , '#~2~#' => ')' );
          $data = strtr( $data , $replace );
        }
        elseif( strpos( $info['text'] , '(' ) !== false )
        {
          // Case where text look like
          // (Soupe Savoyarde gratin\216e au fromage de Raclette au lait cr\236)
          
          // Replace escaped parentheses
          $replace = array( '\\(' => '#~1~#' , '\\)' => '#~2~#' );
          if( preg_match( '~\\(([^\\)]+)\\)~' , strtr( $info['text'] , $replace ) , $matches ) )
          {
            $data .= $matches[1];
          }
          // Restore escaped parentheses
          $replace = array( '#~1~#' => '(' , '#~2~#' => ')' );
          $data = strtr( $data , $replace );
        }
        elseif( strpos( $info['text'] , '<' ) !== false )
        {
          // Case where text look like
          // <0036002900340032>
          if( preg_match( '~<([\\dA-F]+)>~' , $info['text'] , $matches ) )
          {
            $data .= $matches[1];
          }
        }
        
        if( $data )
        {
          // New line appears first
          if( $info['newline'] )
          {
            // Add new line
            $this->_text .= "\n";
          }

          // Then the text
          if( $info['font'] && ( $font = $this->_getFontByName( $info['font'] ) ) )
          {
//            echo 'Data '.htmlspecialchars($info['text']).' converted to \''.$font->__invoke( $data ).'\' helping font name '.$info['font'].'<br /><br />';
            // Apply font
            $this->_text .= $font->__invoke( $data );
          }
          else
          {
            // Translate text in UTF-8
            $this->_text .= iconv( 'ISO-8859-1' , 'UTF-8//TRANSLIT//IGNORE' , $data );
          }
        }
      }
      // Automatically add a new line on this text
      $this->_text .= "\n";
    }
    return $this->_text;
  }

  /**
   * Magic function on string cast
   *
   * @return string
   * @access public
   */
  public function __toString()
  {
    return $this->getText();
  }

}
