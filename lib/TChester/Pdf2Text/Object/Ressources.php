<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage ressources Pdf object
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @author Suriya Kaewmungmuang <sc450617@hotmail.com>
 * @subversion $Id: Ressources.php 8 2011-09-09 15:00:13Z loops $
 */
class TChester_Pdf2Text_Object_Ressources extends TChester_Pdf2Text_Object
{
  /**
   * Font map
   *
   * array(
   *   Font name => Font key ,
   *   Font name => Font key ,
   * )
   *
   * @var array
   * @access protected
   */
  protected $_font_map = array();

  /**
   * Constuctor
   *
   * @param &TChester_Pdf2Text_Objects_Collection $collection
   * @param string $key
   * @param string $dictionary
   * @param string $contents
   * @access public
   */
  public function __construct( TChester_Pdf2Text_Objects_Collection &$collection , $key , $dictionary , $contents )
  {
    parent::__construct( $collection , $key , $dictionary , $contents );

    // Create font map
    $this->_createFontMap();

//    print '<pre>';
//    print 'Ressources key '.$this->_key."\n";
//    print 'Font map:'."\n".htmlspecialchars( var_export( $this->_font_map , true ) )."\n";
//    print '</pre>';
    
  }

  /**
   * Return properly closed status
   *
   * @param none
   * @return string
   * @access public
   */
  public function getFontMap()
  {
    return $this->_font_map;
  }
  
  /**
   * Parse the dictionnary to provide a font map.
   *
   * @param none
   * @return void
   * @access protected
   */
  protected function _createFontMap()
  {
    // Try to found font dictionary
    if( ( $offset = strpos( $this->getDictionary() , '/Font' , 0 ) ) !== false )
    {
      // We need to care about this kind of dictionary
      // /Font 41 0 R /XObject<</Im11 11 0 R/Im12 12 0 R/Im7 7 0 R>> /ProcSet[/PDF/Text/ImageC/ImageI/ImageB] >>
      // where our font dictionnary is <</Im11 11 0 R/Im12 12 0 R/Im7 7 0 R>>
      // but where objects "11 0 R", "12 0 R" and "7 0 R" are fonts but "41 0 R" a FontMap
      
      $fonts_section = substr( $this->getDictionary() , $offset );
      
      $fonts_dictionary = TChester_Pdf2Text_Util::extractDictionary( $fonts_section );
      
      $matches = array();
      
      // It might be the same, case where we did not extract any dictionnary
      if( $fonts_dictionary !== $fonts_section )
      {
        if( preg_match_all( '~/([-.\\w\\d]+)\\s(\\d+\\s\\d+)\\sR~i' , $fonts_dictionary , $matches , PREG_SET_ORDER ) )
        {
          for( $i = 0, $imax = count($matches); $i < $imax; $i++ )
          {
            // Transform object as Font object and assign font name to this object
            if( isset($this->_collection[$matches[$i][2]]) )
            {
              $this->_collection[$matches[$i][2]]->transform( 'Font' );
              $this->_font_map[$matches[$i][1]] = $matches[$i][2];
            }
          }
        }
      }
      
      // Now let's check if there is any fonts map definition
      // Check for "/Font 41 0 R" at the beginning of the /Font section
      if( preg_match( '~^/Font\\s(\\d+\\s\\d+)\\sR~' , $fonts_section , $matches ) )
      {
        // Yeap, there is one
        if( isset($this->_collection[$matches[1]]) )
        {
          $this->_collection[$matches[1]]->transform( 'FontMap' );
          // Merge font maps
          $this->_font_map += $this->_collection[$matches[1]]->getFontMap();
        }
      }
    }
  }

}
