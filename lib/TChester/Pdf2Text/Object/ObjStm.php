<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage Pdf object with undefined status
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @author Suriya Kaewmungmuang <sc450617@hotmail.com>
 * @subversion $Id: ObjStm.php 8 2011-09-09 15:00:13Z loops $
 */
class TChester_Pdf2Text_Object_ObjStm extends TChester_Pdf2Text_Object
{


  /**
   * Constuctor
   *
   * @param &TChester_Pdf2Text_Objects_Collection $collection
   * @param string $key
   * @param string $dictionary
   * @param string $contents
   * @param string $readable_contents
   * @param string $prepend_contents
   * @access public
   */
  public function __construct( TChester_Pdf2Text_Objects_Collection &$collection , $key , $dictionary , $contents )
  {
    parent::__construct( $collection , $key , $dictionary , $contents );

    // Create text map
    $this->_appendObjects();

//    print '<pre>';
//    print 'Text key '.$this->_key."\n";
//    print 'Text map:'."\n";
//    var_export( $this->_text_map );
//    print 'Text contents:'."\n";
//    print htmlspecialchars( $this->_readable_contents );
//    print '</pre>';

  }

  /**
   * Extract object information and append them to the collection
   * 
   * @param none
   * @return void
   * @access public
   */
  protected function _appendObjects()
  {
    $str = $this->getReadableContents();
    
    $matches = array();
    // Extract object key information
    if( preg_match( '~^[0-9\\s]+~' , $str , $matches ) )
    {
      $keys = $matches[0];
      
      // Remove these information from the string
      $str = substr( $str , strlen( $keys ) );
      
      $key1 = strtok( $keys , ' ' );
      $b = strtok( ' ' );
      $continue = true;
      do
      {
        $key2 = strtok( ' ' );
        $e = strtok( ' ' );

        if( $e !== false && $key2 !== false )
        {
          new TChester_Pdf2Text_Object( $this->_collection , $key1.' 0' , substr( $str , $b , $e - $b ) , '' );
          $key1 = $key2;
          $b = $e;
        }
        else
        {
          new TChester_Pdf2Text_Object( $this->_collection , $key1.' 0' , substr( $str , $b ) , '' );
          $continue = false;
        }
      } while( $continue );
    }

  }
}
