<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage Pdf informations data
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: MetaInfo.php 8 2011-09-09 15:00:13Z loops $
 */
class TChester_Pdf2Text_MetaInfo extends TChester_Pdf2Text_Accessor
{
  /**
   * Current collection
   * 
   * @var TChester_Pdf2Text_Objects_Collection
   * @access protected
   */
  protected $_collection;
  
  /**
   * Constructor
   *
   * @param &TChester_Pdf2Text_Objects_Collection $collection
   * @access public
   */
  public function __construct( TChester_Pdf2Text_Objects_Collection &$collection )
  {
    $this->_collection = &$collection;
  }
  
  /**
   * Use an object dictionary to load meta information
   *
   * @param string object key
   * @return boolean
   * @access public
   */
  public function load( $key )
  {
    // Get object
    if( $object = $this->_collection[$key] )
    {
      // Replace escaped parentheses
      $dictionary = strtr( $object->getDictionary() , array( '\\(' => '#~1~#' , '\\)' => '#~2~#' ) );

      $this->title = $this->_extract( $dictionary , 'Title' );
      $this->author = $this->_extract( $dictionary , 'Author' );
      $this->subject = $this->_extract( $dictionary , 'Subject' );
      $this->producer = $this->_extract( $dictionary , 'Producer' );
      $this->creator = $this->_extract( $dictionary , 'Creator' );
      $this->company = $this->_extract( $dictionary , 'Company' );
      $this->source_modified = $this->_extract( $dictionary , 'SourceModified' );
      $this->creation_date = $this->_extract( $dictionary , 'CreationDate' );
      $this->mod_date = $this->_extract( $dictionary , 'ModDate' );
      $this->keywords = $this->_extract( $dictionary , 'Keywords' );
      $this->appl_keywords = $this->_extract( $dictionary , 'AAPL:Keywords' );

      return true;
    }
    return false;
  }

  /**
   * Retrieves a value for a specified key, if the key
   * does not exist an error will be triggered.
   * 
   * @param $name Key to retrieve value for
   * @return mixed Value associated with key or null if error occurred
   * @access public
   */
  public function __get($name)
  {
    $tmp = parent::__get( $name );
    // BOM detection
    // Note that we do nut use mb_strlen()
    if( strlen( $tmp ) > 0 && ord( $tmp{0} ) === 254 && ord( $tmp{1} ) === 255 )
    {
      // BOM found
      // Remove all "\0" character
      // Again, note that we do not use mb_substr()
      $tmp = str_replace( "\0" , '' , substr( $tmp , 2 ) );
    }
    // Sometimes this character is strangely translated...
    // Represented by a character with ASCII code 144 that must be converted
    // to octal 222 that correspond to ’ in WinAnsIEncoding (?!?)
    // For now, I have just one case of that kind...
    // UTF-8 convert this character in 194.144
    return str_replace( chr(194).chr(144) , '’' , iconv( 'ISO-8859-1' , 'UTF-8//TRANSLIT//IGNORE' , $tmp ) );
  }

  /**
   * Extract information for a given key and store them in data array
   *
   * @param $dictionary
   * @param $key
   * @return string
   * @access protected
   */
  protected function _extract( $dictionary , $key  )
  {
    $matches = array();
    // Keywords is represented by another object
    if( preg_match( '~/'.preg_quote($key,'~').'\\s?(\\d+\\s\\d+)\\sR~' , $dictionary, $matches ) )
    {
      if( $object = $this->_collection[$matches[1]] )
      {
        return $object->getReadableContents();
      }
    }
    // Keywords is represented by a string
    elseif( preg_match( '~/'.preg_quote($key,'~').'\\s?\\(([^)]+)\\)~', $dictionary, $matches ) )
    {
      // Restore escaped parentheses
      return strtr( $matches[1] , array( '#~1~#' => '(' , '#~2~#' => ')' ) );
    }
    // Keywords is represented some characters encapsulated in <>
    elseif( preg_match( '~/'.preg_quote($key,'~').'\\s?<([^>]+)>~', $dictionary, $matches ) )
    {
      // Convert with HexaUnicode
      return TChester_Pdf2Text_Decoder_HexaUnicode::getInstance()->__invoke( $matches[1] );
    }
    return false;
  }

  /**
   * Try to found an object that can correspond to information data
   *
   * @param none
   * @return boolean
   * @access public
   */
  public function automatic()
  {
    foreach( $this->_collection as $key => $object )
    {
      $dict = $object->getDictionary();
      if( strpos( $dict , '/Author' ) !== false
       && strpos( $dict , '/Title' ) !== false
       && strpos( $dict , '/ModDate' ) !== false
       && strpos( $dict , '/Creator' ) !== false )
      {
        $this->load( $key );
        return true;
      }
    }
    return false;
  }
}
