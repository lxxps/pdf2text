<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Base class to decode PDF Octal character tag.
 *
 * @author     Thomas Chester
 * @author     Thibaut Zancanaro <t.zancanaro@gmail.com>
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @abstract
 * @subversion $Id: Octal.php 6 2010-09-09 13:00:40Z loops $
 */
abstract class TChester_Pdf2Text_Decoder_Octal implements TChester_Pdf2Text_Decoder_Interface
{

  /**
   * Character table
   *
   * Key is PDF octal value (as string), value is the character.
   * Charaters not present in this table can be converted using
   * iconv( 'ISO-8859-1' , 'UTF-8//TRANSLIT//IGNORE' , chr( octdec( $key ) ) ).
   *
   * @var array
   * @access protected
   * @see http://www.adobe.com/devnet/pdf/pdfs/PDFReference.pdf
   */
  protected $_table = array();

  /**
   * Detect if the string contain one character of PDF set
   *
   * @param  string $str
   * @return boolean
   * @access public
   */
  public function detect( $str )
  {
    // Note double backslashes uses
    return preg_match( '~\\\\[0-7]{3}~' , $str );
  }

  /**
   * Invoker
   *
   * @param  string $str
   * @return string
   * @access public
   */
  public function __invoke( $str )
  {
    // Note double backslashes uses
    return preg_replace_callback( '~\\\\[0-7]{3}~' , array( $this , '_convert' ) , $str );
  }
  
  /**
   * Convert a character to is UTF-8 equivalence
   * 
   * @param array $matches
   * @return string
   * @access protected
   */
  protected function _convert( array $matches )
  {
    // 0 should be the code
    // Escape first backslashes
    $code = substr( $matches[0] , 1 );
    if( isset($this->_table[$code]) )
    {
      return $this->_table[$code];
    }
    // Retrieve the code as is
    return '\\'.$code;
  }
}
