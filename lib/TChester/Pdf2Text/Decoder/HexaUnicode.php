<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Used to decode UTF-16 hexadecimal character from CMap to UTF-8.
 *
 * Some character of the CMap cannot be converted in UTF-8 using classic
 * function, this class helps for that.
 *
 * @author     Thomas Chester
 * @author     Thibaut Zancanaro <t.zancanaro@gmail.com>
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @author     Suriya Kaewmungmuang <sc450617@hotmail.com>
 * @subversion $Id: HexaUnicode.php 8 2011-09-09 15:00:13Z loops $
 */
class TChester_Pdf2Text_Decoder_HexaUnicode implements TChester_Pdf2Text_Decoder_Interface
{
  /**
   * Current TChester_Pdf2Text_Decoder_PDFDocEncoding instance.
   *
   * @var TChester_Pdf2Text_Decoder_PDFDocEncoding Current instance
   * @access protected
   * @static
   */
  protected static $_instance;

  /**
   * Character table
   *
   * Key is UTF-16 hexadecimal octal value (as string), value is the character.
   * Charaters not present in this table can be converted using
   * iconv( 'UTF-16' , 'UTF-8//TRANSLIT//IGNORE' , chr( hexdec( substr( $code , 0 , 2 ) ) ).chr( hexdec( substr( $code , 2 , 4 ) ) ) )
   *
   * @var array
   * @access protected
   * @see http://www.adobe.com/devnet/pdf/pdfs/PDFReference.pdf
   */
  protected $_table = array(
    'F0AB' => '★' , // ★ or ↔ Which one to use? in a PDF ★, but on unicode table ↔
    'F027' => '☎' , // Black phone
    '007F' => '' , // Delete character
    'FFFD' => ' ' , // Replacement character (we don't want to use this one)
  );
  
  /**
   * Method to retrieve TChester_Pdf2Text_Decoder_PDFDocEncoding instance.
   *
   * @param none
   * @return h2aPDFSetDecoder Current instance
   * @access public
   * @static
   */
  public static function getInstance()
  {
  	if( ! self::$_instance instanceof self )
  	{
  		self::$_instance = new self();
  	}
  	return self::$_instance;
  }

  /**
   * Detect if the string contain one character of PDF set
   *
   * @param  string $str
   * @return boolean
   * @access public
   */
  public function detect( $str )
  {
    // Note double backslashes uses
    return preg_match( '~[0-9A-F]{4}~' , $str );
  }

  /**
   * Invoker
   *
   * @param  string $str
   * @return string
   * @access public
   */
  public function __invoke( $str )
  {
    return preg_replace_callback( '~[0-9A-F]{4}~' , array( $this , '_convert' ) , $str );
  }

  /**
   * Convert a character to is UTF-8 equivalence
   *
   * @param array $matches
   * @return string
   * @access protected
   */
  protected function _convert( array $matches )
  {
    // 0 should be the code
    $code = $matches[0];
    
    if( ! isset($this->_table[$code]) )
    {
      // Seems to give the same result... need more inspection
      $this->_table[$code] = iconv( 'UTF-16' , 'UTF-8//TRANSLIT//IGNORE' , chr( hexdec( substr( $code , 0 , 2 ) ) ).chr( hexdec( substr( $code , 2 , 2 ) ) ) );
      // $this->_table[$code] = html_entity_decode( '&#x'.$matches[0].';', ENT_COMPAT, 'UTF-8' );
    }
    return $this->_table[$code];
  }
  
}
