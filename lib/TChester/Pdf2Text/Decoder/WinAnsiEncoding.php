<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Used to decode ASCII PDF character in PDF to UTF-8.
 *
 * Some ASCII value changed from PDF to UTF-8, so this class convert them in
 * the other ASCII value, that is more portable.
 *
 * @author     Thomas Chester
 * @author     Thibaut Zancanaro <t.zancanaro@gmail.com>
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: WinAnsiEncoding.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Pdf2Text_Decoder_WinAnsiEncoding extends TChester_Pdf2Text_Decoder_Octal
{
  /**
   * Current TChester_Pdf2Text_Decoder_PDFDocEncoding instance.
   *
   * @var TChester_Pdf2Text_Decoder_PDFDocEncoding Current instance
   * @access protected
   * @static
   */
  protected static $_instance;

  /**
   * Character table
   *
   * Key is PDF octal value (as string), value is the character.
   * Charaters not present in this table can be converted using
   * iconv( 'ISO-8859-1' , 'UTF-8//TRANSLIT//IGNORE' , chr( octdec( $key ) ) ).
   *
   * @var array
   * @access protected
   * @see http://www.adobe.com/devnet/pdf/pdfs/PDFReference.pdf
   */
  protected $_table = array(

//A A 101 101 101 101
'306' => 'Æ' , //Æ AE 341 256 306 306
'301' => 'Á' , //Á Aacute — 347 301 301
'302' => 'Â' , //Â Acircumflex — 345 302 302
'304' => 'Ä' , //Ä Adieresis — 200 304 304
'300' => 'À' , //À Agrave — 313 300 300
'305' => 'Å' , //Å Aring — 201 305 305
'303' => 'Ã' , //Ã Atilde — 314 303 303
//B B 102 102 102 102
//C C 103 103 103 103
'307' => 'Ç' , //Ç Ccedilla — 202 307 307
//D D 104 104 104 104
//E E 105 105 105 105
'311' => 'É' , //É Eacute — 203 311 311
'312' => 'Ê' , //Ê Ecircumflex — 346 312 312
'313' => 'Ë' , //Ë Edieresis — 350 313 313
'310' => 'È' , //È Egrave — 351 310 310
'320' => 'Ð' , //Ð Eth — — 320 320
'200' => '€' , //€ Euro1 — — 200 240
//F F 106 106 106 106
//G G 107 107 107 107
//H H 110 110 110 110
//I I 111 111 111 111
'315' => 'Í' , //Í Iacute — 352 315 315
'316' => 'Î' , //Î Icircumflex — 353 316 316
'317' => 'Ï' , //Ï Idieresis — 354 317 317
'314' => 'Ì' , //Ì Igrave — 355 314 314
//J J 112 112 112 112
//K K 113 113 113 113
//L L 114 114 114 114
//Ł Lslash 350 — — 225
//M M 115 115 115 115
//N N 116 116 116 116
'321' => 'Ñ' , //Ñ Ntilde — 204 321 321
//O O 117 117 117 117
'214' => 'Œ' , //Œ OE 352 316 214 226
'323' => 'Ó' , //Ó Oacute — 356 323 323
'324' => 'Ô' , //Ô Ocircumflex — 357 324 324
'326' => 'Ö' , //Ö Odieresis — 205 326 326
'322' => 'Ò' , //Ò Ograve — 361 322 322
'330' => 'Ø' , //Ø Oslash 351 257 330 330
'325' => 'Õ' , //Õ Otilde — 315 325 325
//P P 120 120 120 120
//Q Q 121 121 121 121
//R R 122 122 122 122
//S S 123 123 123 123
'212' => 'Š' , //Š Scaron — — 212 227
//T T 124 124 124 124
'336' => 'Þ' , //Þ Thorn — — 336 336
//U U 125 125 125 125
'332' => 'Ú' , //Ú Uacute — 362 332 332
'333' => 'Û' , //Û Ucircumflex — 363 333 333
'334' => 'Ü' , //Ü Udieresis — 206 334 334
'331' => 'Ù' , //Ù Ugrave — 364 331 331
//V V 126 126 126 126
//W W 127 127 127 127
//X X 130 130 130 130
//Y Y 131 131 131 131
'335' => 'Ý' , //Ý Yacute — — 335 335
'237' => 'Ÿ' , //Ÿ Ydieresis — 331 237 230
//Z Z 132 132 132 132
'216' => 'Ž' , //Ž Zcaron2 — — 216 231
//a a 141 141 141 141
'341' => 'á' , //á aacute — 207 341 341
'342' => 'â' , //â acircumflex — 211 342 342
'264' => '´' , //´ acute 302 253 264 264
'344' => 'ä' , //ä adieresis — 212 344 344
'346' => 'æ' , //æ ae 361 276 346 346
'340' => 'à' , //à agrave — 210 340 340
//& ampersand 046 046 046 046
'345' => 'å' , //å aring — 214 345 345
//^ asciicircum 136 136 136 136
//~ asciitilde 176 176 176 176
//* asterisk 052 052 052 052
//@ at 100 100 100 100
'343' => 'ã' , //ã atilde — 213 343 343
//b b 142 142 142 142
//\ backslash 134 134 134 134
//| bar 174 174 174 174
//{ braceleft 173 173 173 173
//} braceright 175 175 175 175
//[ bracketleft 133 133 133 133
//] bracketright 135 135 135 135
//˘ breve 306 371 — 030
'246' => '¦' , //¦ brokenbar — — 246 246
'225' => '•' , //• bullet3 267 245 225 200
//c c 143 143 143 143
//ˇ caron 317 377 — 031
'347' => 'ç' , //ç ccedilla — 215 347 347
'270' => '¸' , //¸ cedilla 313 374 270 270
//¢ cent 242 242 242 242
'210' => 'ˆ' , //ˆ circumflex 303 366 210 032
//: colon 072 072 072 072
//, comma 054 054 054 054
'251' => '©' , //© copyright — 251 251 251
'244' => '¤' , //¤ currency1 250 333 244 244
//d d 144 144 144 144
'206' => '†' , //† dagger 262 240 206 201
'207' => '‡' , //‡ daggerdbl 263 340 207 202
'260' => '°' , //° degree — 241 260 260
'250' => '¨' , //¨ dieresis 310 254 250 250
'367' => '÷' , //÷ divide — 326 367 367
//$ dollar 044 044 044 044
//˙ dotaccent 307 372 — 033
//ı dotlessi 365 365 — 232
//e e 145 145 145 145
'351' => 'é' , //é eacute — 216 351 351
'352' => 'ê' , //ê ecircumflex — 220 352 352
'353' => 'ë' , //ë edieresis — 221 353 353
'350' => 'è' , //è egrave — 217 350 350
//8 eight 070 070 070 070
'205' => '…' , //… ellipsis 274 311 205 203
'227' => '—' , //— emdash 320 321 227 204
'226' => '–' , //– endash 261 320 226 205
//= equal 075 075 075 075
'360' => 'ð' , //ð eth — — 360 360
//! exclam 041 041 041 041
//¡ exclamdown 241 301 241 241
//f f 146 146 146 146
//ﬁ fi 256 336 — 223
//5 five 065 065 065 065
//ﬂ fl 257 337 — 224
'203' => 'ƒ' , //ƒ florin 246 304 203 206
//4 four 064 064 064 064
//⁄ fraction 244 332 — 207
//g g 147 147 147 147
'337' => 'ß' , //ß germandbls 373 247 337 337
'140' => '`' , //` grave 301 140 140 140
//> greater 076 076 076 076
//« guillemotleft4 253 307 253 253
//» guillemotright4 273 310 273 273
'213' => '‹' , //‹ guilsinglleft 254 334 213 210
'233' => '›' , //› guilsinglright 255 335 233 211
//h h 150 150 150 150
//˝ hungarumlaut 315 375 — 034
//- hyphen5 055 055 055 055
//i i 151 151 151 151
'355' => 'í' , //í iacute — 222 355 355
'356' => 'î' , //î icircumflex — 224 356 356
'357' => 'ï' , //ï idieresis — 225 357 357
'354' => 'ì' , //ì igrave — 223 354 354
//j j 152 152 152 152
//k k 153 153 153 153
//l l 154 154 154 154
//< less 074 074 074 074
'254' => '¬' , //¬ logicalnot — 302 254 254
//ł lslash 370 — — 233
//m m 155 155 155 155
'257' => '¯' , //¯ macron 305 370 257 257
//− minus — — — 212
'265' => 'μ' , //μ mu — 265 265 265
'327' => '×' , //× multiply — — 327 327
//n n 156 156 156 156
//9 nine 071 071 071 071
'361' => 'ñ' , //ñ ntilde — 226 361 361
//# numbersign 043 043 043 043
//o o 157 157 157 157
'363' => 'ó' , //ó oacute — 227 363 363
'364' => 'ô' , //ô ocircumflex — 231 364 364
'366' => '/ö' , //ö odieresis — 232 366 366
'234' => 'œ' , //œ oe 372 317 234 234
//˛ ogonek 316 376 — 035
'362' => 'ò' , //ò ograve — 230 362 362
//1 one 061 061 061 061
'275' => '½' , //½ onehalf — — 275 275
'274' => '¼' , //¼ onequarter — — 274 274
'271' => '¹' , //¹ onesuperior — — 271 271
'252' => 'ª' , //ª ordfeminine 343 273 252 252
'272' => 'º' , //º ordmasculine 353 274 272 272
'370' => 'ø' , //ø oslash 371 277 370 370
'365' => 'õ' , //õ otilde — 233 365 365
//p p 160 160 160 160
//¶ paragraph 266 246 266 266
//( parenleft 050 050 050 050
//) parenright 051 051 051 051
//% percent 045 045 045 045
//. period 056 056 056 056
'267' => '·' , //· periodcentered 264 341 267 267
'211' => '‰' , //‰ perthousand 275 344 211 213
//+ plus 053 053 053 053
'261' => '±' , //± plusminus — 261 261 261
//q q 161 161 161 161
//? question 077 077 077 077
//¿ questiondown 277 300 277 277
//" quotedbl 042 042 042 042
'204' => '„' , //„ quotedblbase 271 343 204 214
'223' => '“' , //“ quotedblleft 252 322 223 215
'224' => '”' , //” quotedblright 272 323 224 216
'221' => '‘' , //‘ quoteleft 140 324 221 217
'222' => '’' , //’ quoteright 047 325 222 220
'202' => '‚' , //‚ quotesinglbase 270 342 202 221
'047' => '\'' , //' quotesingle 251 047 047 047
//r r 162 162 162 162
'256' => '®' , //® registered — 250 256 256
//˚ ring 312 373 — 036
//s s 163 163 163 163
'232' => 'š' , //š scaron — — 232 235
//§ section 247 244 247 247
//; semicolon 073 073 073 073
//7 seven 067 067 067 067
//6 six 066 066 066 066
/// slash 057 057 057 057
//space6 040 040 040 040
//£ sterling 243 243 243 243
//t t 164 164 164 164
'376' => 'þ' , //þ thorn — — 376 376
//3 three 063 063 063 063
'276' => '¾' , //¾ threequarters — — 276 276
'263' => '³' , //³ threesuperior — — 263 263
'230' => '˜' , //˜ tilde 304 367 230 037
'231' => '™' , //™ trademark — 252 231 222
//2 two 062 062 062 062
'262' => '²' , //² twosuperior — — 262 262
//u u 165 165 165 165
'372' => 'ú' , //ú uacute — 234 372 372
'373' => 'û' , //û ucircumflex — 236 373 373
'374' => 'ü' , //ü udieresis — 237 374 374
'371' => 'ù' , //ù ugrave — 235 371 371
//_ underscore 137 137 137 137
//v v 166 166 166 166
//w w 167 167 167 167
//x x 170 170 170 170
//y y 171 171 171 171
'375' => 'ý' , //ý yacute — — 375 375
'377' => 'ÿ' , //ÿ ydieresis — 330 377 377
//¥ yen 245 264 245 245
//z z 172 172 172 172
'236' => 'ž' , //ž zcaron2 — — 236 236
//0 zero 060 060 060 060

  );

  /**
   * Method to retrieve TChester_Pdf2Text_Decoder_PDFDocEncoding instance.
   *
   * @param none
   * @return h2aPDFSetDecoder Current instance
   * @access public
   * @static
   */
  public static function getInstance()
  {
  	if( ! self::$_instance instanceof self )
  	{
  		self::$_instance = new self();
  	}
  	return self::$_instance;
  }

}
