<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class used to manage Pdf handle
 *
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Handle.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Pdf2Text_File_Handle
{
  /**
   * File handle
   *
   * @var resource
   * @access protected
   */
  protected $_handle;

  /**
   * File name
   *
   * @var string
   * @access protected
   */
  protected $_name;

  /**
   * Current line
   *
   * @var integer
   * @access protected
   */
  protected $_line = 0;

  /**
   * Current buffer
   *
   * @var string
   * @access protected
   */
  protected $_buffer = '';

  /**
   * Constructor
   *
   * @param string $filepath
   * @return void
   * @access public
   */
  public function __construct( $file )
  {
    $this->_name = $file;
    $this->_handle = fopen( $file, 'rb' );

    if( ! $this->_handle )
    {
      throw new InvalidArgumentException( sprintf( 'Unable to open file "%s"' , $file ) );
    }
  }

  /**
   * Destructor
   * Free resource
   *
   * @param none
   * @return void
   * @access public
   */
  public function __destruct()
  {
    fclose( $this->_handle );
  }

  /**
   * Get current buffer
   *
   * @param none
   * @return string
   * @access public
   */
  public function getBuffer()
  {
    return $this->_buffer;
  }

  /**
   * End of file
   *
   * @param none
   * @return boolean
   * @access public
   */
  public function eof()
  {
    return feof($this->_handle);
  }

  /**
   * Read the next line
   *
   * @param none
   * @return string
   * @access public
   */
  public function readLine()
  {
    if( ! $this->eof() )
    {
      $this->_buffer = fgets($this->_handle);
      $this->_line++;
      return $this->_buffer;
    }
    return false;
  }
  
  /**
   * Read from current buffer to a stop pattern.
   * 
   * @param string $pattern_stop
   * @return string
   * @access public
   */
  public function readUntil($pattern_stop)
  {
    $buffer = '';
    do
    {
      $buffer .= $this->getBuffer();

      if( preg_match( $pattern_stop, trim( $this->getBuffer() ) ) )
        break;

    } while( $this->readLine() );

    return $buffer;
  }
}
