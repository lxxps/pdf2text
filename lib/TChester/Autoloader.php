<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to manage TChester plugin autoloading.
 * 
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Autoloader.php 6 2010-09-09 13:00:40Z loops $
 */
class TChester_Autoloader
{
  /**
   * Current TChester_Autoloader instance.
   *
   * @var TChester_Autoloader Current instance
   * @access protected
   * @static
   */
  protected static $_instance;
  
  /**
   * Root directory.
   *
   * @var string
   * @access protected
   */
  protected $_root;
  
  /**
   * Method to retrieve TChester_Autoloader instance.
   *
   * @param none
   * @return TChester_Autoloader Current instance
   * @access public
   * @static
   */ 
  public static function getInstance()
  {
  	if( empty(self::$_instance) )
  	{
  		self::$_instance = new self();
  	}
  	return self::$_instance;
  }
  
  /**
   * TChester_Autoloader constructor.
   * Protected constructor to force use of getInstance() static method.
   *
   * @param string $dir Base directory
   * @return void
   * @access protected
   */
  protected function __construct()
  {
  	$this->_root = dirname( dirname( __FILE__ ) );
  	spl_autoload_register( array( $this , '_autoload' ) );
  }
  
  /**
   * Method to autoload class.
   *
   * @param string $class Class name
   * @return void
   * @access protected
   */
  protected function _autoload( $class )
  {
    if( strpos( $class , 'TChester' ) === 0 && is_file( $file = $this->_root.DIRECTORY_SEPARATOR.str_replace( '_' , DIRECTORY_SEPARATOR , $class ).'.php' ) )
    {
      require $file;
    }
  }

}