<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * Class to extract text contents and metadata out of a document that was
 * created using the Adobe Portable Document Format.
 *
 * WARNING: The PDF 1.4 specification allows incremental updates
 * to the PDF which could result in appearance of multiple body, cross-
 * reference, and trailer sections. This class has not been tested using
 * such a PDF file and the output of the public interfaces is unknown. It
 * is assumed this incremental updating is the exception
 * rather than the norm.
 * 
 * NOTE: Encryption of PDF's is not supported, contents and metadata will
 * not be available.
 *
 * Here is an example showing how to get the contents and metadata:
 * <code>
 *    $object   = new TChester_Pdf2Text("document1.pdf");
 *    if ($trailer->encrypt === false) {
 *        $contents = $object->getContents();
 *        $title    = $object->getTitle();
 *        $author   = $object->getAuthor();
 *        $subject  = $object->getSubject();
 *        $keywords = $object->getKeywords();
 *        $creator  = $object->getCreator();
 *        $producer = $object->getProducer();
 *        $created  = $object->getCreationDate();
 *        $modified = $object->getModDate();
 *    }
 * </code>
 *
 * @link http://www.adobe.com/devnet/pdf/pdf_reference.html PDF Reference
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @author Suriya Kaewmungmuang <sc450617@hotmail.com>
 * @subversion $Id: Pdf2Text.php 8 2011-09-09 15:00:13Z loops $
 */
class TChester_Pdf2Text implements TChester_Pdf2Text_MetaInfo_Interface, TChester_Pdf2Text_Structure_Interface
{
  /**
   * Current version
   *
   * @var string
   * @const
   */
  const VERSION = '2.0.3';

  /**
   * The PDF text contents from the body section
   * @var string
   * @access protected
   */
  protected $_contents;

  /**
   * The contents of the PDF header section
   * @var TChester_Pdf2Text_Bag_Header
   * @access protected
   */
  protected $_bagHeader;

  /**
   * The contents of the PDF trailer section
   * @var TChester_Pdf2Text_Bag_Trailer
   * @access protected
   */
  protected $_bagTrailer;

  /**
   * The contents of the PDF body section
   * @var TChester_Pdf2Text_Bag_Body
   * @access protected
   */
  protected $_bagBody;

  /**
   * The contents of the PDF cross reference section
   *
   * @var TChester_Pdf2Text_Bag_Xref
   * @access protected
   */
  protected $_bagXref;

  /**
   * PDF meta informations
   *
   * @var TChester_Pdf2Text_MetaInfo
   * @access protected
   */
  protected $_metaInfo;

  /**
   * Class constructor
   *
   * @param string $filename Name and path of PDF file
   * @return void
   * @access public
   */
  public function __construct($filename)
  {
    // Without this setting, trying to parse Windows created PDF's on Mac/Unix
    // or vice versa will not work correctly.
    ini_set( 'auto_detect_line_endings' , true );

    // We are going to suppress errors while executing certain code statements
    // but we still want to preserve any errors for output.
    ini_set( 'track_errors' , true );

    $this->_bagHeader  = new TChester_Pdf2Text_Bag_Header();
    $this->_bagTrailer = new TChester_Pdf2Text_Bag_Trailer();
    $this->_bagBody    = new TChester_Pdf2Text_Bag_Body();
    $this->_bagXref    = new TChester_Pdf2Text_Bag_Xref();

    $handle = new TChester_Pdf2Text_File_Handle( $filename );
    $handle->readLine();
    $this->_bagHeader->extract($handle);

    while( ! $handle->eof() )
    {
      $handle->readLine();
      $this->_bagTrailer->extract($handle);
      $this->_bagBody->extract($handle);
      $this->_bagXref->extract($handle);
    }

    // All object have been extracted
    if( ! $this->_bagTrailer->seen )
    {
      // Trailer not found, probably in the first part (1024 bytes) of the file
    }

    // Destroy handle
    unset($handle);

    // Launch analyzer (this part work with reference)
    $collection = $this->_bagBody->getCollection();
    new TChester_Pdf2Text_Objects_Analyzer( $collection );

    $this->_metaInfo = new TChester_Pdf2Text_MetaInfo( $collection );
    if( $info = $this->_bagTrailer->info )
    {
      $this->_metaInfo->load( $info );
    }
    else
    {
      // Try to found an object where dictionnay could be usable
      $this->_metaInfo->automatic();
    }

  }

  /**
   * Return current MetaInfo object
   *
   * @param none
   * @return TChester_Pdf2Text_MetaInfo
   * @access public
   */
  public function getMetaInfo()
  {
    return $this->_metaInfo;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getTitle()
   * @access public
   */
  public function getTitle()
  {
    return $this->_metaInfo->title;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getAuthor()
   * @access public
   */
  public function getAuthor()
  {
    return $this->_metaInfo->author;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getSubject()
   * @access public
   */
  public function getSubject()
  {
    return $this->_metaInfo->subject;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getKeywords()
   * @access public
   */
  public function getKeywords()
  {
    return $this->_metaInfo->keywords;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getCreator()
   * @access public
   */
  public function getCreator()
  {
    return $this->_metaInfo->creator;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getProducer()
   * @access public
   */
  public function getProducer()
  {
    return $this->_metaInfo->producer;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getCreationDate()
   * @access public
   */
  public function getCreationDate()
  {
    return $this->_metaInfo->creation_date;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getModDate()
   * @access public
   */
  public function getModDate()
  {
    return $this->_metaInfo->mod_date;
  }

  /**
   * Supports the TChester_Pdf2Text_MetaInfo_Interface interface
   *
   * @param none
   * @return string
   * @see TChester_Pdf2Text_MetaInfo_Interface::getContents()
   * @access public
   */
  public function getContents()
  {
    if( $this->_contents === null )
    {
      $this->_contents = '';

      if( $this->_bagTrailer->encrypt !== true )
      {
        foreach( $this->_bagBody->getCollection()->getObjects() as $object )
        {
          $this->_contents .= (string)$object;
        }
      }
    }
    return $this->_contents;
  }

  /**
   * Supports the TChester_Pdf2Text_Structure_Interface interface
   *
   * @param none
   * @return TChester_Pdf2Text_Bag_Header
   * @see TChester_Pdf2Text_Structure_Interface::getHeader()
   * @access public
   */
  public function getHeader()
  {
      return $this->_bagHeader;
  }

  /**
   * Supports the TChester_Pdf2Text_Structure_Interface interface
   *
   * @param none
   * @return TChester_Pdf2Text_Bag_Trailer
   * @see TChester_Pdf2Text_Structure_Interface::getTrailer()
   * @access public
   */
  public function getTrailer()
  {
      return $this->_bagTrailer;
  }

  /**
   * Supports the TChester_Pdf2Text_Structure_Interface interface
   *
   * @param none
   * @return TChester_Pdf2Text_Bag_Body
   * @see TChester_Pdf2Text_Structure_Interface::getBody()
   * @access public
   */
  public function getBody()
  {
      return $this->_bagBody;
  }

  /**
   * Supports the TChester_Pdf2Text_Structure_Interface interface
   *
   * @param none
   * @return TChester_Pdf2Text_Bag_Xref
   * @see TChester_Pdf2Text_Structure_Interface::getXref()
   * @access public
   */
  public function getXref()
  {
      return $this->_bagXref;
  }
    
}
