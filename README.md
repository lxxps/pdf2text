# **Pdf2Text**

Pdf2Text is an extended version of the Pdf2Text project initiated by 
Thomas Chester on Launchpad (see https://launchpad.net/pdf2text).


## Requirements:

PHP 5.2.14 should be enough. Nothing really special, works with basic functions.

By convenience, Pdf2Text extract all texts in UTF-8.


## How to use it:

At first, call bootstrap:

    :::php
      require $path_to_library.'/TChester/bootstrap.inc.php';

Then, extract content from a PDF file:

    :::php
      $pdf = new TChester_Pdf2Text( $pdf_file_path );
      $contents = $pdf->getContents();

	  
## Examples:

Put all the package to a `pdf2text` folder on your local server,
then you will be able to see Pdf2Text in action at `http://localhost/pdf2text/demo/index.php`.

If you want to test a particular PDF file, just put it in `samples` folder and run 
extraction from this demo.


## Unit testing:

Unit testing for this project is not good at all. It just give us ability to check 
if there is not any major fails on updates.