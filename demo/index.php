<?php
/**
 * Copyright 2010, Thomas Chester
 * Copyright 2011-2014, Pierrot Evrard
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Pdf2Text
 * @author Thomas Chester
 * @author Pierrot Evrard <pierrotevrard@gmail.com>
 * @link https://launchpad.net/pdf2text First version of the project on Launchpad
 * @link https://bitbucket.org/lxxps/pdf2text Pdf2Text on Bitbucket
 * @copyright Copyright 2010, Thomas Chester
 * @copyright Copyright 2011-2014, Pierrot Evrard
 * @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @version 2.0.2
 */

/**
 * This file is normally used to view a specific PDF file.
 *
 * @subversion $Id: samples.php 9 2011-09-09 15:17:10Z loops $
 */
	error_reporting( E_ALL );
	ini_set( 'display_errors' , true );
	ini_set( 'html_errors' , false );
  
  // Do not hesitate to clear the APC cache if you made some changes
  // apc_clear_cache();
  
  header('Content-Type: text/html; charset=utf-8');
  
  define( 'SAMPLES_DIRECTORY' , dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'samples'.DIRECTORY_SEPARATOR );

  $file_name = isset($_GET['f']) ? $_GET['f'] : null;
?>

<html>
<head><title>PDF-2-Text</title></head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
  label { font-weight: bold; float: left; width: 200px; }
  p { clear: both; }
  pre { border: 1px black solid; padding: 20px; }
</style>
<body>

<?php if( $file_name ):

require_once( '../lib/TChester/bootstrap.inc.php' );

$pdf = new TChester_Pdf2Text( SAMPLES_DIRECTORY.$file_name );
$pdf->getContents();

?>

<h1>PDF2Text on file "<?php echo $file_name; ?>"</h1>

<hr>

<h2>PDF Header</h2>

<label>Header:</label> <?php echo htmlspecialchars($pdf->getHeader()->header); ?><br />
<label>Version:</label> <?php echo htmlspecialchars($pdf->getHeader()->version); ?><br />

<hr>

<h2>PDF Body</h2>

<?php foreach( $pdf->getBody()->getCollection() as $obj ): ?>

  <br />

  <label>Key:</label> <?php echo htmlspecialchars( $obj->getKey() ); ?><br />
  <label>Dictionary:</label><?php echo htmlspecialchars( $obj->getDictionary() ); ?><br />
  <label>Class:</label> <?php echo htmlspecialchars( get_class( $obj ) ); ?><br />

  <?php if( $obj instanceof TChester_Pdf2Text_Object_Contents ): ?>
  
    <label>Has text:</label> <?php echo var_export( $obj->hasText() , true ); ?><br />
    <?php if( $obj->hasText() ): ?>
      <label>Contents:</label>
      <p><?php echo htmlspecialchars( (string)$obj ); ?></p>
    <?php endif; ?>

  <?php elseif( $obj instanceof TChester_Pdf2Text_Object_Font ): ?>

    <label>Font encoding:</label> <?php echo $obj->getEncoding(); ?><br />
    <label>Has CMap:</label> <?php echo $obj->hasCMap() ? 'Yes' : 'No'; ?><br />

  <?php elseif( $obj instanceof TChester_Pdf2Text_Object_Ressources ): ?>

    <label>Font map:</label> <?php $sep = ''; foreach( $obj->getFontMap() as $font => $key ): echo $sep.$font.': '.$key; $sep = ', '; endforeach; ?><br />

  <?php endif; ?>

<?php endforeach; ?>

<br />

<hr>

<h2>PDF Trailer</h2>

<label>Dictionary (Raw):</label> <?php echo htmlspecialchars($pdf->getTrailer()->dictionary); ?><br />
<label>Dictionary (Id1):</label> <?php echo htmlspecialchars($pdf->getTrailer()->id1); ?><br />
<label>Dictionary (Id2):</label> <?php echo htmlspecialchars($pdf->getTrailer()->id2); ?><br />
<label>Dictionary (Root):</label> <?php echo htmlspecialchars($pdf->getTrailer()->root); ?><br />
<label>Dictionary (Info):</label> <?php echo htmlspecialchars($pdf->getTrailer()->info); ?><br />
<label>Dictionary (Size):</label> <?php echo htmlspecialchars($pdf->getTrailer()->size); ?><br />
<label>Dictionary (Prev):</label> <?php echo htmlspecialchars($pdf->getTrailer()->prev); ?><br />
<label>Dictionary (Encrypt):</label> <?php echo ( $pdf->getTrailer()->encrypt === true ? 'Yes' : 'No' ); ?><br />
<label>Start Xref Offset:</label> <?php echo htmlspecialchars($pdf->getTrailer()->startXref); ?><br />
<label>EOF:</label> <?php echo htmlspecialchars($pdf->getTrailer()->eof); ?><br />

<hr>

<h2>PDF Info</h2>

<label>Title:</label> <?php echo htmlspecialchars($pdf->getTitle()); ?><br />
<label>Author:</label> <?php echo htmlspecialchars($pdf->getAuthor()); ?><br />
<label>Subject:</label> <?php echo htmlspecialchars($pdf->getSubject()); ?><br />
<label>Keywords:</label> <?php echo htmlspecialchars($pdf->getKeywords()); ?><br />
<label>Creator:</label> <?php echo htmlspecialchars($pdf->getCreator()); ?><br />
<label>Producer:</label> <?php echo htmlspecialchars($pdf->getProducer()); ?><br />
<label>CreationDate:</label> <?php echo htmlspecialchars($pdf->getCreationDate()); ?><br />
<label>ModDate:</label> <?php echo htmlspecialchars($pdf->getModDate()); ?><br />

<hr>

<h2>PDF Contents</h2>
<p>
<?php if ( $pdf->getTrailer()->encrypt === true ): ?>
Contents are not available because PDF is encrypted.
<?php else: ?>
	<?php echo htmlspecialchars( $pdf->getContents() ); ?>
<?php endif; ?>
</p>

<?php if ( $pdf->getTrailer()->encrypt !== true ): ?>
<h3>Formatted contents <small>(to use on result file)</small></h3>
<pre>
<?php	echo htmlspecialchars( $pdf->getContents() ); ?>
</pre>
<?php endif; ?>

<hr />

<?php endif; // endif( $file_name )

$samples = glob( SAMPLES_DIRECTORY.'*' );

?>


<h1>List of available samples</h1>
<ul>
  <?php foreach( $samples as $file ): ?>
  <li><a href="?f=<?php echo basename($file); ?>"><?php echo basename($file); ?></a></li>
  <?php endforeach; ?>
</ul>

</body>
</html>